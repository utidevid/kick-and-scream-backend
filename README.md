kickscream
==========

A Symfony project created on July 20, 2017, 6:48 pm.


To run the project

1. `docker-compose up -d`
2. `composer install`
3. `bin/console doctrine:database:create`
4. `bin/console doctrine:migrations:migrate`
5. `bin/console doctrine:fixtures:load --fixtures=src/AppBundle/DataFixtures/ORM/EssentialData.php`
6. `bin/console doctrine:fixtures:load --fixtures=src/AppBundle/DataFixtures/ORM/EssentialData.php`
7. Set up bundle: https://github.com/lexik/LexikJWTAuthenticationBundle/blob/master/Resources/doc/index.md#prerequisites
8. Create a custom user for tests `bin/console fos:user:custom_create Raleigh Ale root raleigh.ale@gmail.com root`
9. `vendor/bin/phpunit -c app/phpunit.xml.dist` to run integration tests
10. `vendor/bin/behat --suite=rest` to run BDD tests

To generate token: /app/token and post a basic authorization username and password
Token is only valid for an hour

Run on dev to create fixtures bin/console `bin/console hautelook:fixtures:load`