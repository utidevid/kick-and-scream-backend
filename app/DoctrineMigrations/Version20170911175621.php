<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170911175621 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, username_canonical VARCHAR(180) NOT NULL, email VARCHAR(180) NOT NULL, email_canonical VARCHAR(180) NOT NULL, enabled TINYINT(1) NOT NULL, salt VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, last_login DATETIME DEFAULT NULL, confirmation_token VARCHAR(180) DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', fname VARCHAR(50) NOT NULL, lname VARCHAR(255) NOT NULL, api_key VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D64992FC23A8 (username_canonical), UNIQUE INDEX UNIQ_8D93D649A0D96FBF (email_canonical), UNIQUE INDEX UNIQ_8D93D649C05FB297 (confirmation_token), UNIQUE INDEX UNIQ_8D93D649C912ED9D (api_key), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, object_id VARCHAR(10) DEFAULT NULL, title VARCHAR(255) NOT NULL, language VARCHAR(2) NOT NULL, sku VARCHAR(20) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_64C19C1232D562B (object_id), UNIQUE INDEX UNIQ_64C19C12B36786B (title), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE challenges (id INT AUTO_INCREMENT NOT NULL, game_user_id INT DEFAULT NULL, category_id INT NOT NULL, object_id VARCHAR(10) DEFAULT NULL, title VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, points SMALLINT NOT NULL, gender SMALLINT NOT NULL, min_players_count SMALLINT NOT NULL, code SMALLINT NOT NULL, type SMALLINT NOT NULL, source SMALLINT NOT NULL, rating INT DEFAULT NULL, moderated TINYINT(1) NOT NULL, native_id INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_7B5A7E0232D562B (object_id), UNIQUE INDEX UNIQ_7B5A7E02B36786B (title), INDEX IDX_7B5A7E032ACACBA (game_user_id), INDEX IDX_7B5A7E012469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE game_user (id INT AUTO_INCREMENT NOT NULL, object_id VARCHAR(10) NOT NULL, primary_email VARCHAR(255) NOT NULL, device_name VARCHAR(255) NOT NULL, device_id VARCHAR(255) NOT NULL, run_count BIGINT DEFAULT NULL, game_played_count BIGINT DEFAULT NULL, last_game_played DATETIME DEFAULT NULL, last_logged_in DATETIME DEFAULT NULL, locale VARCHAR(5) DEFAULT NULL, timezone SMALLINT DEFAULT NULL, possible_accounts LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json_array)\', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_6686BA65232D562B (object_id), UNIQUE INDEX UNIQ_6686BA657284A5A9 (primary_email), UNIQUE INDEX UNIQ_6686BA6594A4C7D4 (device_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE game_user_social_profile (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, fb_auth_data_json LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json_array)\', fb_id BIGINT DEFAULT NULL, fb_email VARCHAR(255) DEFAULT NULL, fb_link VARCHAR(255) DEFAULT NULL, name VARCHAR(100) DEFAULT NULL, gender SMALLINT DEFAULT NULL, social_network SMALLINT NOT NULL, UNIQUE INDEX UNIQ_9E9E9B9AD87F565A (fb_id), UNIQUE INDEX UNIQ_9E9E9B9AB3E71AD2 (fb_email), UNIQUE INDEX UNIQ_9E9E9B9A9D9D1607 (fb_link), INDEX IDX_9E9E9B9AA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE challenges ADD CONSTRAINT FK_7B5A7E032ACACBA FOREIGN KEY (game_user_id) REFERENCES game_user (id)');
        $this->addSql('ALTER TABLE challenges ADD CONSTRAINT FK_7B5A7E012469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE game_user_social_profile ADD CONSTRAINT FK_9E9E9B9AA76ED395 FOREIGN KEY (user_id) REFERENCES game_user (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE challenges DROP FOREIGN KEY FK_7B5A7E012469DE2');
        $this->addSql('ALTER TABLE challenges DROP FOREIGN KEY FK_7B5A7E032ACACBA');
        $this->addSql('ALTER TABLE game_user_social_profile DROP FOREIGN KEY FK_9E9E9B9AA76ED395');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE challenges');
        $this->addSql('DROP TABLE game_user');
        $this->addSql('DROP TABLE game_user_social_profile');
    }
}
