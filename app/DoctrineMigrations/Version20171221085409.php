<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171221085409 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE time_zone (id INT AUTO_INCREMENT NOT NULL, cc VARCHAR(2) DEFAULT NULL, coordinates VARCHAR(16) DEFAULT NULL, timezone VARCHAR(50) NOT NULL, comments LONGTEXT DEFAULT NULL, format VARCHAR(100) NOT NULL, utc_offset VARCHAR(6) NOT NULL, utc_dst_offset VARCHAR(6) NOT NULL, notes LONGTEXT DEFAULT NULL, UNIQUE INDEX UNIQ_D56C92ADBB21A79 (cc), UNIQUE INDEX UNIQ_D56C92A3701B297 (timezone), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE time_zone');
    }
}
