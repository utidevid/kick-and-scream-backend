<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171221170445 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE game_user CHANGE timezone timezone INT DEFAULT NULL');
        $this->addSql('ALTER TABLE game_user ADD CONSTRAINT FK_6686BA653701B297 FOREIGN KEY (timezone) REFERENCES time_zone (id)');
        $this->addSql('CREATE INDEX IDX_6686BA653701B297 ON game_user (timezone)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE game_user DROP FOREIGN KEY FK_6686BA653701B297');
        $this->addSql('DROP INDEX IDX_6686BA653701B297 ON game_user');
        $this->addSql('ALTER TABLE game_user CHANGE timezone timezone SMALLINT DEFAULT NULL');
    }
}
