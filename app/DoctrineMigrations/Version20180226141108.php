<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180226141108 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE facebook_time_zone (id INT AUTO_INCREMENT NOT NULL, fb_id INT NOT NULL, name VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_D59C4363D87F565A (fb_id), UNIQUE INDEX UNIQ_D59C43635E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE time_zone ADD facebook_time_zone_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE time_zone ADD CONSTRAINT FK_D56C92AE3D7335 FOREIGN KEY (facebook_time_zone_id) REFERENCES facebook_time_zone (fb_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D56C92AE3D7335 ON time_zone (facebook_time_zone_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE time_zone DROP FOREIGN KEY FK_D56C92AE3D7335');
        $this->addSql('DROP TABLE facebook_time_zone');
        $this->addSql('DROP INDEX UNIQ_D56C92AE3D7335 ON time_zone');
        $this->addSql('ALTER TABLE time_zone DROP facebook_time_zone_id');
    }
}
