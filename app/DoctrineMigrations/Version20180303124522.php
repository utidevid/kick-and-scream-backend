<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180303124522 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE time_zone DROP FOREIGN KEY FK_D56C92AE3D7335');
        $this->addSql('ALTER TABLE time_zone ADD CONSTRAINT FK_D56C92AE3D7335 FOREIGN KEY (facebook_time_zone_id) REFERENCES facebook_time_zone (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE time_zone DROP FOREIGN KEY FK_D56C92AE3D7335');
        $this->addSql('ALTER TABLE time_zone ADD CONSTRAINT FK_D56C92AE3D7335 FOREIGN KEY (facebook_time_zone_id) REFERENCES facebook_time_zone (fb_id)');
    }
}
