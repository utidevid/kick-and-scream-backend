<?php

use AppBundle\Command\Utils\CustomUserManipulator;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\TableNode;
use Behat\Symfony2Extension\Context\KernelDictionary;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\Common\Util\Inflector;
use GuzzleHttp\Middleware;
use Imbo\BehatApiExtension\Context\ApiContext;
use Psr\Http\Message\RequestInterface;
use RestBundle\Entity\Category;
use RestBundle\Entity\Challenge;
use RestBundle\Entity\GameUser;
use RestBundle\Entity\TimeZone;

require_once __DIR__ . '/../../vendor/phpunit/phpunit/src/Framework/Assert/Functions.php';

/**
 * Defines application features from the specific context.
 */
class FeatureRestContext extends ApiContext implements Context
{
    use KernelDictionary;

    /**
     * @var \AppBundle\Entity\User
     */
    private $currentUser;

    /**
     * @var array $createdTimezones - list of created timezones
     */
    private $createdTimezones = array();

    protected function addTimezoneToList(TimeZone $timezone)
    {
        $this->createdTimezones[] = $timezone;
    }

    /**
     * Get timezone by timezone
     *
     * @param $timezone
     * @return mixed|null
     */
    protected function getTimezoneByTimezone($timezone) {
        foreach ($this->createdTimezones as $item) {
            if ($item->getTimezone() == $timezone) {
                return $item;
            }
        }

        return null;
    }

    protected function isTimezoneCreated(TimeZone $timezone)
    {
        foreach ($this->createdTimezones as $item) {
            if ($timezone->isEqual($item)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    private function createAdminUser($username = 'root', $password = 'root')
    {
        $this->currentUser = $this->getContainer()->get(CustomUserManipulator::class)
            ->createCustomUser('Andy', 'Pandy', $username, $password, 'andy.zaporozhets@gmail.com', true, false);
    }

    /**
     * @return \AppBundle\Entity\User
     */
    protected function getCurrentUser()
    {
        return $this->currentUser;
    }

    private function getEntityManager()
    {
        return $this->getContainer()->get('doctrine.orm.entity_manager');
    }

    /**
     * Get authorization token
     *
     * @param string $username
     * @return string
     * @throws \Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTEncodeFailureException
     */
    public function  getAuthorizationToken($username = 'root')
    {
        return $this->getContainer()->get('lexik_jwt_authentication.encoder')
            ->encode(['username' => $username]);
    }

    /**
     * @BeforeScenario
     * @throws \Doctrine\DBAL\DBALException
     */
    public function purgeDatabase()
    {
        // Tables with foreign keys cannot be truncated
        $conn = $this->getEntityManager()->getConnection();

        $statement = $conn->prepare('SET FOREIGN_KEY_CHECKS = 0;');
        $statement->execute();

        $purger = new ORMPurger($this->getContainer()->get('doctrine')->getManager());
        $purger->setPurgeMode(ORMPurger::PURGE_MODE_TRUNCATE);
        $purger->purge();

        $statement = $conn->prepare('SET FOREIGN_KEY_CHECKS = 1;');
        $statement->execute();

    }

    /**
     * @BeforeScenario
     */
    public function prepareDatabase()
    {
        $this->createAdminUser();
    }

    /**
     * @BeforeScenario
     */
    public function adjustRequest()
    {
        $stack = $this->client->getConfig('handler');
        $stack->push(Middleware::mapRequest(function (RequestInterface $request) {
            $path = $request->getUri()->getPath();

            if (strpos($path, '/api') === 0) {
                return $request->withUri($request->getUri()->withPath('/app_test.php' . $path))
                    ->withAddedHeader('Authorization', 'Bearer ' . $this->getAuthorizationToken());
            }

            return $request;
        }));
    }

    /**
     * @Given there is/are :count challenge(s)
     * @param $count
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function thereAreChallenges($count)
    {
        $category = $this->getContainer()->get(Category::class);
        $gameUser = $this->getContainer()->get(GameUser::class);

        $em = $this->getEntityManager();
        $em->persist($category);
        $em->persist($gameUser);

        for ($i = 0; $i < $count; $i++) {
            $challenge = $this->getContainer()->get(Challenge::class);
            $challenge->setCategory($category);
            $challenge->setGameUser($gameUser);

            $em->persist($challenge);
        }

        $em->flush();
    }

    /**
     * @Given there are/is :count categories/category
     * @param $count
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function thereAreCategories($count)
    {
        $this->createNumberOfData($count, Category::class);
    }

    /**
     * @Given there are/is (a) categories/category created:
     * @param TableNode $table
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function thereAreCategoriesCreated(TableNode $table)
    {
        $this->createDataFromTable($table, Category::class);
    }

    /**
     * @Then the following category/categories exist(s):
     * @param TableNode $table
     */
    public function theFollowingCategoriesExist(TableNode $table)
    {
        $this->assertFollowingData($table, Category::class);
    }

    /**
     * @Given there are/is (a) following in-game user(s) created:
     * @param TableNode $table
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function thereAreFollowingInGameUsersCreated(TableNode $table)
    {
        $this->createDataFromTable($table, GameUser::class,
            function ($key, $value) {
                if (in_array($key, ['last_game_played', 'last_logged_in'])) {
                    return date_create_from_format('Y-m-d H:i:s', $value);
                }

                if ($key == 'timezone') {
                    $timezone = $this->getContainer()->get(TimeZone::class);
                    $timezone->setTimezone($value);

                    if ($this->isTimezoneCreated($timezone)) {
                        $timezone = $this->getTimezoneByTimezone($value);
                    } {
                        $em = $this->getEntityManager();
                        $em->persist($timezone);
                        $em->flush();

                        $this->addTimezoneToList($timezone);
                    }

                    return $timezone;
                }

                return $value;
            });
    }

    /**
     * @Given there are/is (a) following timezone(s) created:
     * @param TableNode $table
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function thereAreFollowingTimezonesCreated(TableNode $table)
    {
        $this->createDataFromTable($table, TimeZone::class);
    }

    /**
     * @Given the following in-game user(s) exist(s):
     * @param TableNode $table
     */
    public function theFollowingInGameUsersExist(TableNode $table)
    {
        $this->assertFollowingData($table, GameUser::class);
    }

    /**
     * @Given there are/is :count in-game user(s)
     * @param $count
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function thereAreInGameUsers($count)
    {
        $this->createNumberOfData($count, GameUser::class);
    }

    /**
     * Create the data from the table
     *
     * @param TableNode $table
     * @param string $entityClassName - e.g. MyEntity:class
     * @param callable|null $callback - modify table row value by column name
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function createDataFromTable(TableNode $table, $entityClassName, callable $callback = null)
    {
        $em = $this->getEntityManager();

        foreach ($table as $key => $row) {
            $gameUser = $this->getContainer()->get($entityClassName);

            foreach (array_keys($row) as $key) {
                $setter = 'set' . ucfirst(Inflector::camelize($key));

                $value = $row[$key];

                if ($callback) {
                    $value = $callback($key, $value);
                }

                $gameUser->$setter($value);
            }

            $em->persist($gameUser);
        }

        $em->flush();
    }

    /**
     * Check if the following data exist in the database
     *
     * @param TableNode $table
     * @param string $entityClassName  - e.g. MyEntity:class
     */
    protected function assertFollowingData(TableNode $table, $entityClassName)
    {
        $em = $this->getEntityManager();

        foreach ($table as $row) {
            $entity = $em->getRepository($entityClassName)->find($row['id']);
            assertNotNull($entity, 'No record with ID ' . $row['id'] . ' exists in ' . $entityClassName);
        }
    }

    /**
     * Create a number of data
     *
     * @param int $count - how many of data should be created
     * @param string $entityClassName - what type of data should be created, e.g. MyEntity:class
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function createNumberOfData($count, $entityClassName)
    {
        $em = $this->getEntityManager();

        for ($i = 0; $i < $count; $i++) {
            $entity = $this->getContainer()->get($entityClassName);
            $em->persist($entity);
        }

        $em->flush();
    }

    /**
     * @Then the response body is empty
     */
    public function theResponseBodyIsEmpty()
    {
        assertEmpty($this->response->getBody()->getContents(), 'Response body is not empty.');
    }
}
