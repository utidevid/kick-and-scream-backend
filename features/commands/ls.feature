Feature: ls
  In order to see the directory structure
  As a UNIX user
  I need to be able to list the current directory's content

  Background:
    Given there is a file named "john"

  Scenario: List 2 files in a directory
    Given there is a file named "jane"
    When I run "ls"
    Then I should see "john" in the output
    And I should see "jane" in the output

  Scenario: List 1 file in 1 directory
    Given there is a dir named "names"
    When I run "ls"
    Then I should see "john" in the output
    And I should see "names" in the output