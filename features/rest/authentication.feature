Feature: Authentication
  In order to gain access to the API
  As an API user
  I need to be able to receive a token

  Scenario: Receiving a token
    Given the following form parameters are set:
        | name | value |
        | _username | root |
        | _password | root |
    When I request "/api/token" using HTTP "POST"
    Then the response status line is "200 OK"
    And the "Content-type" response header is "application/json"
    And the response body contains JSON:
        """
        {
          "token": "@variableType(string)"
        }
        """
