Feature: Categories API resource
  In order to manage categories that group challenges
  As an API user
  I need to be able to create categories and link challenges to them

  Scenario: List available categories
    Given there are "5" categories
    When I request "/api/categories" using HTTP "GET"
    Then the response body is a JSON array of length "5"
    And the response status line is "200 OK"
    And the "Content-type" response header is "application/hal+json"

  Scenario: Get a specific category
    Given there are categories created:
      | title | object_id | language | sku |
      | Adult | 70d2f43861 | en | cat_1 |
      | Crazy | 70d2f43862 | en | cat_2 |
      | In a pub | 70d2f43863 | en | cat_3 |
    When I request "/api/categories/3" using HTTP "GET"
    Then the response body contains JSON:
      """
      {
        "id": 3,
        "object_id": "70d2f43863",
        "title": "In a pub",
        "language": "en",
        "sku": "cat_3",
        "created_at": "@variableType(string)",
        "updated_at": "@variableType(string)",
        "_links": {
              "self": {
                  "href": "@variableType(string)"
              }
          }
      }
      """
    And the response status line is "200 OK"
    And the "Content-type" response header is "application/hal+json"

  Scenario: Get a non-existent category
    Given there are categories created:
      | title | object_id | language | sku |
      | Adult | 70d2f43861 | en | cat_1 |
      | Crazy | 70d2f43862 | en | cat_2 |
    When I request "/api/categories/3" using HTTP "GET"
    Then the response body contains JSON:
      """
      {
        "status": 404,
        "type": "about:blank",
        "title": "Not Found",
        "detail": "A Category entity has not been found by \"3\" ID",
        "entity": {
            "name": "Category",
            "id": "3"
        }
      }
      """
    And the response status line is "404 Not Found"
    And the "Content-type" response header is "application/problem+json"

  Scenario: Create a new category
    Given the request body is:
      """
      {
        "title": "Test Category",
        "language": "en",
        "sku": "cat_1",
        "object_id": "abc123xyz4"
      }
      """
    When I request "/api/categories" using HTTP "POST"
    Then the response body contains JSON:
      """
      {
        "id": 1,
        "object_id": "abc123xyz4",
        "title": "Test Category",
        "language": "en",
        "sku": "cat_1",
        "created_at": "@variableType(string)",
        "updated_at": "@variableType(string)",
        "_links": {
              "self": {
                  "href": "@variableType(string)"
              }
          }
      }
      """
    And the response status line is "201 Created"
    And the "Content-type" response header is "application/hal+json"

  Scenario: Create a duplicate category
    Given the request body is:
        """
        {
          "title": "Test 1",
          "language": "en",
          "sku": "cat_1",
          "object_id": "abc123xyz4"
        }
        """
    And there is a category created:
      | title | object_id | language | sku |
      | Test 1 | abc123xyz4 | en | cat_1 |
    When I request "/api/categories" using HTTP "POST"
    Then the response body contains JSON:
        """
        {
            "status": 400,
            "type": "validation_error",
            "title": "A validation error occurred",
            "detail": "Submitted data are invalid. Please review them and try again.",
            "errors": {
                "title": [
                    "Such category title is already used. Please come up with a new one."
                ],
                "object_id": [
                    "A category with such object ID already exists. Please add a unique object ID."
                ]
            }
        }
        """
    And the response status line is "400 Bad Request"
    And the "Content-type" response header is "application/problem+json"

  Scenario: Update a category
    Given the request body is:
      """
      {
        "id": 22,
        "title": "Test 2",
        "language": "de",
        "sku": "cat_2",
        "object_id": "new567obj4",
        "created_at": "2007-11-17",
        "updated_at": "2007-11-17"
      }
      """
    And there is a category created:
      | title | object_id | language | sku |
      | Test 1 | abc123xyz4 | en | cat_1 |
    When I request "/api/categories/1" using HTTP "PATCH"
    Then the response body contains JSON:
      """
      {
        "id": 1,
        "object_id": "abc123xyz4",
        "title": "Test 2",
        "language": "de",
        "sku": "cat_2",
        "created_at": "@regExp(/^((?!2007-11-17).)*$/)",
        "updated_at": "@regExp(/^((?!2007-11-17).)*$/)",
        "_links": {
              "self": {
                  "href": "@variableType(string)"
              }
          }
      }
      """
    And the response status line is "200 OK"
    And the "Content-type" response header is "application/hal+json"

  Scenario: Partially update a category
    Given the request body is:
      """
      {
        "language": "ua"
      }
      """
    And there is a category created:
      | title | object_id | language | sku |
      | Test 1 | abc123xyz4 | en | cat_1 |
    When I request "/api/categories/1" using HTTP "PATCH"
    Then the response body contains JSON:
      """
      {
        "id": 1,
        "object_id": "abc123xyz4",
        "title": "Test 1",
        "language": "ua",
        "sku": "cat_1",
        "created_at": "@variableType(string)",
        "updated_at": "@variableType(string)",
        "_links": {
              "self": {
                  "href": "@variableType(string)"
              }
          }
      }
      """
    And the response status line is "200 OK"
    And the "Content-type" response header is "application/hal+json"

  Scenario: Update a category with data that already exist in different category
    Given the request body is:
        """
        {
          "title": "Test 2",
          "object_id": "abc123xyz5"
        }
        """
    And there is a category created:
      | title | object_id | language | sku |
      | Test 1 | abc123xyz4 | en | cat_1 |
      | Test 2 | abc123xyz5 | en | cat_2 |
    When I request "/api/categories/1" using HTTP "PATCH"
    Then the response body contains JSON:
        """
        {
            "status": 400,
            "type": "validation_error",
            "title": "A validation error occurred",
            "detail": "Submitted data are invalid. Please review them and try again.",
            "errors": {
                "title": [
                    "Such category title is already used. Please come up with a new one."
                ]
            }
        }
        """
    And the response status line is "400 Bad Request"
    And the "Content-type" response header is "application/problem+json"

  Scenario: Update a non-existent category:
    Given the request body is:
      """
      {
        "id": 22,
        "title": "Test 2",
        "language": "de",
        "sku": "cat_2"
      }
      """
    And there is a category created:
      | title | object_id | language | sku |
      | Test 1 | abc123xyz4 | en | cat_1 |
    When I request "/api/categories/23" using HTTP "PATCH"
    Then the response body contains JSON:
    """
    {
        "status": 404,
        "type": "about:blank",
        "title": "Not Found",
        "detail": "A Category entity has not been found by \"23\" ID",
        "entity": {
            "name": "Category",
            "id": "23"
        }
    }
    """
    And the response status line is "404 Not Found"
    And the "Content-type" response header is "application/problem+json"

  Scenario: Delete a category
    Given there are categories created:
      | title | object_id | language | sku |
      | Adult | 70d2f43861 | en | cat_1 |
      | Crazy | 70d2f43862 | en | cat_2 |
      | Fantastic | 70d2f43863 | en | cat_3 |
    When I request "/api/categories/2" using HTTP "DELETE"
    Then the response status line is "204 No Content"
    And the response body is empty
    And the following categories exist:
      | id |
      | 1 |
      | 3 |

  Scenario: Delete a non-existent category
    Given there are categories created:
      | title | object_id | language | sku |
      | Adult | 70d2f43861 | en | cat_1 |
    When I request "/api/categories/22" using HTTP "DELETE"
    Then the response body contains JSON:
      """
      {
          "status": 404,
          "type": "about:blank",
          "title": "Not Found",
          "detail": "RestBundle\\Entity\\Category object not found."
      }
      """
    And the response status line is "404 Not Found"
    And the "Content-type" response header is "application/problem+json"