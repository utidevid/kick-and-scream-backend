Feature: Challenge API resource
  In order to manage challenges that are rendered in the Android application
  As an API user
  I need to be able to create/edit/delete challenges

  Scenario: List available challenges
    Given there are "5" challenges
    When I request "/api/challenges" using HTTP "GET"
    Then the response body contains JSON:
        """
        {
          "total": 5,
          "count": 5,
          "items": "@arrayLength(5)"
        }
        """
    And the response status line is "200 OK"
    And the "Content-type" response header is "application/hal+json"

  Scenario Outline: Filter categories by title
    Given there are "<challenges_quantity>" challenges
    When I request "/api/challenges?filter=<filter_text>" using HTTP "GET"
    Then the response body contains JSON:
        """
        {
          "total": <total_count>,
          "count": <count>,
          "items": "@arrayLength(<count>)"
        }
        """
    Examples:
      | filter_text | total_count | count | challenges_quantity |
      | Impressive | 50 | 30 | 50 |
      | Something | 0 | 0 | 50 |
    And the response status line is "200 OK"
    And the "Content-type" response header is "application/hal+json"


