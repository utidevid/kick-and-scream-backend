Feature: Game User API Resource
  In order to manage the challenges of the social category
  As an API User
  I need to be able to create users that have signed up in the mobile application

  Scenario: List available in-game users
    Given there are "50" in-game users
    When I request "/api/gameusers" using HTTP "GET"
    Then the response status line is "200 OK"
    And the "Content-type" response header is "application/hal+json"
    And the response body contains JSON:
    """
    {
      "items": "@arrayLength(30)",
      "total": 50,
      "count": 30
    }
    """

  Scenario: Get a specific in-game user
    Given there are following in-game users created:
      | object_id | primary_email | device_name | device_id | run_count | game_played_count | last_game_played | last_logged_in | locale | timezone | possible_accounts |
      | abc123xyz1 | andypandy@gmail.com | HTC One M8 | f80sdjf0sd93 | 22 | 17 | 2017-10-30 21:29:43 | 2017-10-30 21:29:43 | en | Europe/Kiev | {"facebook":"38459334859"} |
      | abc123xyz2 | alex@gmail.com | Pixel 2 XL | sdfsd333423 | 11 | 77 | 2017-10-30 21:29:43 | 2017-10-30 21:29:43 | en | Europe/Kiev | {"facebook":"3453453477"} |
    When I request "/api/gameusers/2" using HTTP "GET"
    Then the response body contains JSON:
    """
    {
      "id": 2,
      "object_id": "abc123xyz2",
      "primary_email": "alex@gmail.com",
      "device_name": "Pixel 2 XL",
      "device_id": "sdfsd333423",
      "run_count": 11,
      "game_played_count": 77,
      "last_game_played": "2017-10-30 21:29:43",
      "last_logged_in": "2017-10-30 21:29:43",
      "locale": "en",
      "timezone": "@variableType(integer)",
      "possible_accounts": {
        "facebook": "3453453477"
      },
      "created_at": "@variableType(string)",
      "updated_at": "@variableType(string)",
      "_links": {
            "self": {
                "href": "@variableType(string)"
            }
        }
    }
    """
    And the response status line is "200 OK"
    And the "Content-type" response header is "application/hal+json"

  Scenario: Get a non-existent game user
    Given there are following in-game users created:
      | object_id | primary_email | device_name | device_id | run_count | game_played_count | last_game_played | last_logged_in | locale | timezone | possible_accounts |
      | abc123xyz1 | andypandy@gmail.com | HTC One M8 | f80sdjf0sd93 | 22 | 17 | 2017-10-30 21:29:43 | 2017-10-30 21:29:43 | en | Europe/Kiev | {"facebook":"38459334859"} |
      | abc123xyz2 | alex@gmail.com | Pixel 2 XL | sdfsd333423 | 11 | 77 | 2017-10-30 21:29:43 | 2017-10-30 21:29:43 | en | Europe/Kiev | {"facebook":"3453453477"} |
    When I request "/api/gameusers/3" using HTTP "GET"
    Then the response body contains JSON:
      """
      {
        "status": 404,
        "type": "about:blank",
        "title": "Not Found",
        "detail": "A GameUser entity has not been found by \"3\" ID",
        "entity": {
            "name": "GameUser",
            "id": "3"
        }
      }
      """
    And the response status line is "404 Not Found"
    And the "Content-type" response header is "application/problem+json"

  Scenario: Create a new in-game user
    Given there is a following timezone created:
      | timezone |
      | America/Havana |
    And the request body is:
      """
      {
        "object_id": "abc123xyz1",
        "primary_email": "alex@gmail.com",
        "device_name": "Pixel 2 XL",
        "device_id": "sdfsd333423",
        "run_count": 11,
        "game_played_count": 77,
        "last_game_played": "2017-10-30 21:29:43",
        "last_logged_in": "2017-10-30 21:29:43",
        "locale": "en_GB",
        "timezone": 1,
        "possible_accounts": {
          "facebook": "3453453477",
          "telegram": "alex33"
        }
      }
      """
    When I request "/api/gameusers" using HTTP "POST"
    Then the response body contains JSON:
      """
      {
        "id": 1,
        "object_id": "abc123xyz1",
        "primary_email": "alex@gmail.com",
        "device_name": "Pixel 2 XL",
        "device_id": "sdfsd333423",
        "run_count": null,
        "game_played_count": null,
        "last_game_played": null,
        "last_logged_in": null,
        "locale": "en_GB",
        "timezone": 1,
        "possible_accounts": {
          "facebook": "3453453477",
          "telegram": "alex33"
        },
        "created_at": "@variableType(string)",
        "updated_at": "@variableType(string)",
        "_links": {
              "self": {
                  "href": "@variableType(string)"
              }
          }
      }
      """
    And the response status line is "201 Created"
    And the "Content-type" response header is "application/hal+json"

  Scenario: Create a duplicate in-game user
    Given there is a following timezone created:
      | timezone |
      | America/Havana |
    And the request body is:
      """
      {
        "object_id": "abc123xyz1",
        "primary_email": "alex@gmail.com",
        "device_name": "Pixel 2 XL",
        "device_id": "sdfsd333423",
        "locale": "en_GB",
        "timezone": 1,
        "possible_accounts": {
          "facebook": "3453453477",
          "telegram": "alex33"
        }
      }
      """
    And there is a following in-game user created:
      | object_id | primary_email | device_name | device_id | run_count | game_played_count | last_game_played | last_logged_in | locale | timezone | possible_accounts |
      | abc123xyz1 | alex@gmail.com | Pixel 2 XL | sdfsd333423 | 11 | 77 | 2017-10-30 21:29:43 | 2017-10-30 21:29:43 | en_GB | 1 | {"facebook":"3453453477"} |
    When I request "/api/gameusers" using HTTP "POST"
    Then the response body contains JSON:
        """
        {
            "status": 400,
            "type": "validation_error",
            "title": "A validation error occurred",
            "detail": "Submitted data are invalid. Please review them and try again.",
            "errors": {
                "primary_email": [
                    "This email is already used. Please come up with a new one."
                ],
                "object_id": [
                    "A game user with such object ID already exists. Please add a unique object ID."
                ]
            }
        }
        """
    And the response status line is "400 Bad Request"
    And the "Content-type" response header is "application/problem+json"

  Scenario: Update an in-game user
    Given there is a following timezone created:
      | timezone |
      | America/Havana |
      | America/Havana2 |
    And the request body is:
      """
      {
        "id": 15,
        "object_id": "gdfgdfg",
        "primary_email": "alex+11@gmail.com",
        "device_name": "Nexus 22",
        "device_id": "hahaha77373",
        "run_count": 5,
        "game_played_count": 3,
        "last_game_played": "2027-10-30 21:29:43",
        "last_logged_in": "2027-10-30 21:29:43",
        "locale": "de_GR",
        "timezone": 2,
        "possible_accounts": {
          "facebook": "444"
        },
        "created_at": "2018-01-03",
        "updated_at": "2018-01-04"
      }
      """
    And there is a following in-game user created:
      | object_id | primary_email | device_name | device_id | run_count | game_played_count | last_game_played | last_logged_in | locale | timezone | possible_accounts |
      | abc123xyz2 | alex@gmail.com | Pixel 2 XL | sdfsd333423 | 11 | 77 | 2017-10-30 21:29:43 | 2017-10-30 21:29:43 | en_US | 1 | {"facebook":"3453453477"} |
    When I request "/api/gameusers/1" using HTTP "PATCH"
    Then the response body contains JSON:
      """
      {
        "id": 1,
        "object_id": "abc123xyz2",
        "primary_email": "alex+11@gmail.com",
        "device_name": "Nexus 22",
        "device_id": "hahaha77373",
        "run_count": 5,
        "game_played_count": 3,
        "last_game_played": "2027-10-30 21:29:43",
        "last_logged_in": "2027-10-30 21:29:43",
        "locale": "de_GR",
        "timezone": 2,
        "possible_accounts": {
          "facebook": "444"
        },
        "created_at": "@variableType(string)",
        "updated_at": "@variableType(string)",
        "_links": {
              "self": {
                  "href": "@variableType(string)"
              }
          }
      }
      """
    And the response status line is "200 OK"
    And the "Content-type" response header is "application/hal+json"

  Scenario: Partially update an in-game user
    Given the request body is:
      """
      {
        "run_count": 8
      }
      """
    And there is a following timezone created:
      | timezone |
      | America/Havana |
    And there is a following in-game user created:
      | object_id | primary_email | device_name | device_id | run_count | game_played_count | last_game_played | last_logged_in | locale | timezone | possible_accounts |
      | abc123xyz2 | alex@gmail.com | Pixel 2 XL | sdfsd333423 | 7 | 77 | 2017-10-30 21:29:43 | 2017-10-30 21:29:43 | en_US | 1 | {"facebook":"3453453477"} |
    When I request "/api/gameusers/1" using HTTP "PATCH"
    Then the response body contains JSON:
      """
      {
        "id": 1,
        "object_id": "abc123xyz2",
        "primary_email": "alex@gmail.com",
        "device_name": "Pixel 2 XL",
        "device_id": "sdfsd333423",
        "run_count": 8,
        "game_played_count": 77,
        "last_game_played": "2017-10-30 21:29:43",
        "last_logged_in": "2017-10-30 21:29:43",
        "locale": "en_US",
        "timezone": "@variableType(integer)",
        "possible_accounts": {
          "facebook": "3453453477"
        },
        "created_at": "@variableType(string)",
        "updated_at": "@variableType(string)",
        "_links": {
              "self": {
                  "href": "@variableType(string)"
              }
          }
      }
      """
    And the response status line is "200 OK"
    And the "Content-type" response header is "application/hal+json"

  Scenario: Update an in-game user with data that already exist in different user
    Given the request body is:
        """
        {
          "primary_email": "andy@gmail.com",
          "object_id": "abc123xyz3",
          "device_id": "sdfsd333424"
        }
        """
    And there is a following timezone created:
      | timezone |
      | America/Havana |
    And there is a following in-game user created:
      | object_id | primary_email | device_name | device_id | run_count | game_played_count | last_game_played | last_logged_in | locale | timezone | possible_accounts |
      | abc123xyz2 | alex@gmail.com | Pixel 2 XL | sdfsd333423 | 7 | 77 | 2017-10-30 21:29:43 | 2017-10-30 21:29:43 | en_US | 1 | {"facebook":"3453453477"} |
      | abc123xyz3 | andy@gmail.com | Pixel 2 XL | sdfsd333424 | 7 | 77 | 2017-10-30 21:29:43 | 2017-10-30 21:29:43 | en_US | 1 | {"facebook":"3453453477"} |
    When I request "/api/gameusers/1" using HTTP "PATCH"
    Then the response body contains JSON:
        """
        {
            "status": 400,
            "type": "validation_error",
            "title": "A validation error occurred",
            "detail": "Submitted data are invalid. Please review them and try again.",
            "errors": {
                "primary_email": [
                    "This email is already used. Please come up with a new one."
                ],
                "device_id": [
                    "The device ID should be unique. Two same devices cannot be processed."
                ]
            }
        }
        """
    And the response status line is "400 Bad Request"
    And the "Content-type" response header is "application/problem+json"

  Scenario: Update a non-existent in-game user:
    Given the request body is:
      """
      {
        "run_count": 8
      }
      """
    When I request "/api/gameusers/1" using HTTP "PATCH"
    Then the response body contains JSON:
    """
    {
        "status": 404,
        "type": "about:blank",
        "title": "Not Found",
        "detail": "A GameUser entity has not been found by \"1\" ID",
        "entity": {
            "name": "GameUser",
            "id": "1"
        }
    }
    """
    And the response status line is "404 Not Found"
    And the "Content-type" response header is "application/problem+json"

  Scenario: Delete an in-game user
    Given there is a following timezone created:
      | timezone |
      | America/Havana |
    And there are following in-game users created:
      | object_id | primary_email | device_name | device_id | run_count | game_played_count | last_game_played | last_logged_in | locale | timezone | possible_accounts |
      | abc123xyz1 | andypandy@gmail.com | HTC One M8 | f80sdjf0sd93 | 22 | 17 | 2017-10-30 21:29:43 | 2017-10-30 21:29:43 | en | Europe/Kiev | {"facebook":"38459334859"} |
      | abc123xyz2 | alex@gmail.com | Pixel 2 XL | sdfsd333423 | 11 | 77 | 2017-10-30 21:29:43 | 2017-10-30 21:29:43 | 1 | Europe/Kiev | {"facebook":"3453453477"} |
      | abc123xyz3 | dana@gmail.com | Pixel 3 XL | adfsd333423 | 11 | 77 | 2017-10-30 21:29:43 | 2017-10-30 21:29:43 | 1 | Europe/Kiev | {"facebook":"33"} |
    When I request "/api/gameusers/2" using HTTP "DELETE"
    Then the response status line is "204 No Content"
    And the response body is empty
    And the following in-game users exist:
      | id |
      | 1 |
      | 3 |

  Scenario: Delete a non-existent in-game user
    Given there is a following timezone created:
      | timezone |
      | America/Havana |
    And there are following in-game users created:
      | object_id | primary_email | device_name | device_id | run_count | game_played_count | last_game_played | last_logged_in | locale | timezone | possible_accounts |
      | abc123xyz1 | andypandy@gmail.com | HTC One M8 | f80sdjf0sd93 | 22 | 17 | 2017-10-30 21:29:43 | 2017-10-30 21:29:43 | en_US | 1 | {"facebook":"38459334859"} |
      | abc123xyz2 | alex@gmail.com | Pixel 2 XL | sdfsd333423 | 11 | 77 | 2017-10-30 21:29:43 | 2017-10-30 21:29:43 | en_US | 1 | {"facebook":"3453453477"} |
    When I request "/api/gameusers/22" using HTTP "DELETE"
    Then the response body contains JSON:
      """
      {
          "status": 404,
          "type": "about:blank",
          "title": "Not Found",
          "detail": "RestBundle\\Entity\\GameUser object not found."
      }
      """
    And the response status line is "404 Not Found"
    And the "Content-type" response header is "application/problem+json"