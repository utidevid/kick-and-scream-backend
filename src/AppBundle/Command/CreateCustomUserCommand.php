<?php

namespace AppBundle\Command;

use AppBundle\Command\Utils\CustomUserManipulator;
use AppBundle\Entity\User;
use FOS\UserBundle\Command\CreateUserCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateCustomUserCommand extends CreateUserCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        parent::configure();

        $definition = array_merge(
            array(
                new InputArgument('fname', InputArgument::REQUIRED, 'The firstname'),
                new InputArgument('lname', InputArgument::REQUIRED, 'The lastname'),
            ),
            $this->getDefinition()->getArguments(),
            $this->getDefinition()->getOptions()
        );

        $this->setName('fos:user:custom_create')
            ->setDescription('Create a custom user with fname and lname as first params.')
            ->setDefinition($definition);
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $fname = $input->getArgument('fname');
        $lname = $input->getArgument('lname');
        $username = $input->getArgument('username');
        $email = $input->getArgument('email');
        $password = $input->getArgument('password');
        $inactive = $input->getOption('inactive');
        $superadmin = $input->getOption('super-admin');

        $manipulator = $this->getContainer()->get(CustomUserManipulator::class);
        $manipulator->createCustomUser($fname, $lname, $username, $password, $email, !$inactive, $superadmin);

        $output->writeln(sprintf('Created a custom user <comment>%s</comment>', $username));
    }
}
