<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 9/26/2017
 * Time: 2:00 PM
 */

namespace AppBundle\Command\Utils;


use AppBundle\Entity\User;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Util\UserManipulator;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class CustomUserManipulator extends UserManipulator
{
    /**
     * @var UserManagerInterface
     */
    private $userManager;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;
    /**
     * @var RequestStack
     */
    private $requestStack;
    /**
     * @var User
     */
    private $user;

    public function __construct(UserManagerInterface $userManager, EventDispatcherInterface $dispatcher, RequestStack $requestStack, User $user)
    {
        parent::__construct($userManager, $dispatcher, $requestStack);
        $this->userManager = $userManager;
        $this->dispatcher = $dispatcher;
        $this->requestStack = $requestStack;
        $this->user = $user;
    }

    public function createCustomUser($fname, $lname, $username, $password, $email, $active, $superadmin)
    {
        $this->user->setUsername($username);
        $this->user->setEmail($email);
        $this->user->setPlainPassword($password);
        $this->user->setEnabled((bool) $active);
        $this->user->setSuperAdmin((bool) $superadmin);
        $this->user->setFname($fname);
        $this->user->setLname($lname);

        $this->userManager->updateUser($this->user);

        $event = new UserEvent($this->user, $this->requestStack->getCurrentRequest());
        $this->dispatcher->dispatch(FOSUserEvents::USER_CREATED, $event);

        return $this->user;
    }
}