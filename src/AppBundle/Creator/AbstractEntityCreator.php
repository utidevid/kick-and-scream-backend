<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 19-Dec-17
 * Time: 4:40 PM
 */

namespace AppBundle\Creator;


use Doctrine\ORM\EntityManager;

abstract class AbstractEntityCreator implements EntityCreatorInterface
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritdoc}
     */
    public function create($entity)
    {
        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        return $entity;
    }

    /**
     * {@inheritdoc}
     */
    public function update($entity)
    {
        $this->entityManager->flush($entity);

        return $entity;
    }

    /**
     * {@inheritdoc}
     */
    public function delete($entity)
    {
        $this->entityManager->remove($entity);
        $this->entityManager->flush();
    }
}