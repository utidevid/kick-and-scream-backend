<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 19-Dec-17
 * Time: 4:40 PM
 */

namespace AppBundle\Creator;


interface EntityCreatorInterface
{
    /**
     * Create an entity
     *
     * @param object $entity
     *
     * @return object
     */
    public function create($entity);

    /**
     * Update an entity
     *
     * @param object $entity
     *
     * @return object
     */
    public function update($entity);

    /**
     * Delete an entity
     *
     * @param $entity
     *
     * @return void
     */
    public function delete($entity);
}