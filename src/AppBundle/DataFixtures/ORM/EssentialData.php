<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 21-Dec-17
 * Time: 11:25 AM
 */

namespace AppBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class EssentialData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $env = $this->container->getParameter("kernel.environment");
            
        $loader = new Loader();

        $loader->addFixture(new FacebookTimeZoneData());
        $loader->addFixture(new TimeZoneData());

        if ($env == 'dev') {
            $loader->addFixture(new LoadUserData($this->container->get('security.password_encoder')));
        }

        $purger = new ORMPurger();
        $executor = new ORMExecutor($manager, $purger);
        $executor->execute($loader->getFixtures());
    }

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}