<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 21-Dec-17
 * Time: 9:23 AM
 */

namespace AppBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use RestBundle\Entity\FacebookTimeZone;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Parser;

class FacebookTimeZoneData implements FixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $yaml = new Parser();
        
        try {
            $values = $yaml->parse(file_get_contents(__DIR__ . '/../../../../src/AppBundle/DataFixtures/ORM/data/prod-fb-timezones.yml'));

            foreach ($values as $value) {
                $facebookTimeZone = new FacebookTimeZone();

                $facebookTimeZone->setFbId($value['fb_id']);
                $facebookTimeZone->setName($value['name']);

                $manager->persist($facebookTimeZone);
                $manager->flush();
            }
        } catch (ParseException $e) {
            printf("Unable to parse the YAML string: %s", $e->getMessage());
        }
    }
}