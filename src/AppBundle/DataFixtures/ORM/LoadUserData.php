<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class LoadUserData implements FixtureInterface
{

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $userAdmin = new User();

        $userAdmin->setUsername('root');
        $userAdmin->setEmail('andy.zaporozhets@gmail.com');
        $userAdmin->setRoles(['ROLE_ADMIN']);
        $userAdmin->setFname('Andy');
        $userAdmin->setLname('Pandy');
        $userAdmin->setEnabled(true);
        $userAdmin->setSalt(md5(uniqid()));

        $password = $this->encoder->encodePassword($userAdmin, 'root');
        $userAdmin->setPassword($password);

        $manager->persist($userAdmin);
        $manager->flush();
    }
}