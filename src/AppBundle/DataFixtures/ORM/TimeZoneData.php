<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 21-Dec-17
 * Time: 9:23 AM
 */

namespace AppBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use RestBundle\Entity\TimeZone;
use RestBundle\Repository\FacebookTimeZoneRepository;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Parser;

class TimeZoneData implements FixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $yaml = new Parser();
        
        try {
            $values = $yaml->parse(file_get_contents(__DIR__ . '/../../../../src/AppBundle/DataFixtures/ORM/data/prod-timezones.yml'));

            foreach ($values as $value) {
                $timezone = new TimeZone();

                $timezone->setCc($value['cc']);
                $timezone->setCoordinates($value['coordinates']);
                $timezone->setTimezone($value['timezone']);
                $timezone->setComments($value['comments']);
                $timezone->setFormat($value['format']);
                $timezone->setUtcOffset($value['utc_offset']);
                $timezone->setUtcDstOffset($value['utc_dst_offset']);
                $timezone->setNotes($value['notes']);

                // Find a match in FB time zones
                $fbTimeZoneName = 'TZ_' . strtoupper(str_replace(['/', '-'], '_', $timezone->getTimezone()));

                $fbTimeZone = $manager->getRepository('RestBundle:FacebookTimeZone')
                    ->findOneByName($fbTimeZoneName);

                if ($fbTimeZone) {
                    $timezone->setFacebookTimeZone($fbTimeZone);
                }

                $manager->persist($timezone);
                $manager->flush();

            }
        } catch (ParseException $e) {
            printf("Unable to parse the YAML string: %s", $e->getMessage());
        }
    }
}