<?php

namespace AppBundle\Entity;

/**
 * User
 */
class User extends \FOS\UserBundle\Model\User
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $fname;

    /**
     * @var string
     */
    protected $lname;

    /**
     * @var string
     */
    private $apiKey;

    public function __construct()
    {
        parent::__construct();

        $this->roles = array('ROLE_USER');
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fname
     *
     * @param string $fname
     *
     * @return User
     */
    public function setFname($fname)
    {
        $this->fname = $fname;

        return $this;
    }

    /**
     * Get fname
     *
     * @return string
     */
    public function getFname()
    {
        return $this->fname;
    }

    /**
     * Set lname
     *
     * @param string $lname
     *
     * @return User
     */
    public function setLname($lname)
    {
        $this->lname = $lname;

        return $this;
    }

    /**
     * Get lname
     *
     * @return string
     */
    public function getLname()
    {
        return $this->lname;
    }

    /**
     * @return mixed
     */
    public function getApiToken()
    {
        return $this->apiKey;
    }

    /**
     * @param mixed $apiToken
     */
    public function setApiToken($apiKey)
    {
        $this->apiKey = $apiKey;
    }
}

