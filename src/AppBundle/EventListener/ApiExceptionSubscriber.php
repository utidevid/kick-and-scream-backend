<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 9/24/2017
 * Time: 10:43 PM
 */

namespace AppBundle\EventListener;


use JMS\Serializer\Exception\RuntimeException;
use RestBundle\Api\ApiProblem;
use RestBundle\Api\ApiProblemException;
use RestBundle\Factory\ResponseFactory;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\KernelEvents;

class ApiExceptionSubscriber implements EventSubscriberInterface
{
    private $debug;
    /**
     * @var ResponseFactory
     */
    private $responseFactory;

    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::EXCEPTION => 'onKernelException'
        );
    }

    public function __construct($debug, ResponseFactory $responseFactory)
    {
        $this->debug = $debug;
        $this->responseFactory = $responseFactory;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $request = $event->getRequest();
        // If URL does not start with /api, then continue to normal Symfony exception handling
        if (strpos($request->getPathInfo(), '/api') !== 0) {
            return;
        }

        $exception = $event->getException();

        $statusCode = $exception instanceof HttpException ? $exception->getStatusCode() : 500;

        // JMS Serializer throws a runtime exception when json cannot be parsed
        if ($exception instanceof RuntimeException &&
            strpos($exception->getMessage(), 'Could not decode JSON') !== false) {
            $statusCode = 400;
        }
        else if ($exception instanceof \ErrorException &&
            strpos($exception->getMessage(), 'Notice: Undefined index') !== false) {
            $statusCode = 400;
        }

        // If 500 error, allow normal Symfony exception handling as long as debug mode is enabled
        if ($statusCode == 500 && $this->debug) {
            return;
        }

        if ($exception instanceof ApiProblemException) {
            $apiProblem = $exception->getApiProblem();
        } else {
            $apiProblem = new ApiProblem($statusCode);

            if ($exception instanceof HttpException || $exception->getMessage()) {
                $apiProblem->setDetail($exception->getMessage());
            }
        }

        $response = $this->responseFactory->createProblemResponse($apiProblem);

        $event->setResponse($response);
    }
}