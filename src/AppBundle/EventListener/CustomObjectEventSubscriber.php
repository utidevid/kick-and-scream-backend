<?php

namespace AppBundle\EventListener;

use AppBundle\Exception\ValidationErrorException;
use AppBundle\Serializer\IdAwareDataReplacerDeserializationContext;
use Doctrine\Common\Util\Inflector;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\PreDeserializeEvent;
use JMS\Serializer\Metadata\PropertyMetadata;
use RestBundle\Api\ApiProblem;
use RestBundle\Api\ApiProblemException;

class CustomObjectEventSubscriber implements EventSubscriberInterface
{

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Returns the events to which this class has subscribed.
     *
     * Return format:
     *     array(
     *         array('event' => 'the-event-name', 'method' => 'onEventName', 'class' => 'some-class', 'format' => 'json'),
     *         array(...),
     *     )
     *
     * The class may be omitted if the class wants to subscribe to events of all classes.
     * Same goes for the format key.
     *
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            [
                'event' => 'serializer.pre_deserialize',
                'method' => 'onPreDeserialize',
                'format' => 'json'
            ]
        ];
    }

    public function onPreDeserialize(PreDeserializeEvent $event)
    {
        $className = $event->getType()['name'];

        if (preg_match('/(?:Bundle)\\\\Entity/', $className)) {
            $classMetadata = $event->getContext()->getMetadataFactory()
                ->getMetadataForClass($className)->propertyMetadata;

            $fields = $event->getData();

            if (is_array($fields)) {
                if ($event->getContext() instanceof IdAwareDataReplacerDeserializationContext) {
                    foreach ($classMetadata as $propertyName => $property) {
                        $propertyName = $property->serializedName ?: Inflector::tableize($propertyName);

                        if (!isset($fields[$propertyName])) {
                            $fields[$propertyName] = null;
                        }
                    }

                    $event->setData($fields);
                }

                $invalidFields = array();

                foreach ($fields as $fieldKey => $fieldVal) {
                    $fieldName = Inflector::camelize($fieldKey);
                    /** @var PropertyMetadata $property */
                    $property = $classMetadata[$fieldName];

                    // Do not validate properties which are out of group scope
                    // Prevents created_at and updated_at fields to validate since they are passed here as string
                    if ($event->getContext()->getExclusionStrategy()
                        ->shouldSkipProperty($property, $event->getContext())) {
                        continue;
                    }

                    $fieldRequiredType = $property->type['name'];
                    $fieldCurrentType = gettype($fieldVal);

                    // If required type is an entity, then ask for an integer since it expects an ID to be passed
                    if (strpos($fieldRequiredType, 'Entity') !== false) {
                        $fieldRequiredType = 'integer';
                    }

                    // If required type is a DateTime, then ask for a string
                    // String conversation to DateTime occurs in DateTimeEventSubscriber
                    if (strpos($fieldRequiredType, 'DateTime') !== false) {
                        $fieldRequiredType = 'string';
                    }

                    // Is nullable is different for mapped and actual fields
                    if ($this->isNullable($className, $fieldName) && $fieldCurrentType === 'NULL') {
                        // Everything is okay
                    } else {
                        if($fieldCurrentType !== $fieldRequiredType) {
                            $invalidFields[$fieldName] = [
                                'Field requires ' . $fieldRequiredType .
                                ($this->isNullable($className, $fieldName) ? ' or null' : '') . ' data type. Instead ' . $fieldCurrentType . ' type was passed.'
                            ];
                        }
                    }
                }

                if ($invalidFields) {
                    $apiProblem = new ApiProblem(400, ApiProblem::INVALID_DATA_TYPE);
                    $apiProblem->setDetail('Invalid data type passed. Check whether values match correct type.');
                    $apiProblem->setErrors($invalidFields);

                    throw new ApiProblemException($apiProblem);
                }
            }
        }
    }

    /**
     * Is nullable of Entity Manager checks only actual fields, for mapped fields it throws '500: No mapping found for a key'
     * To avoid this, first actual fields are checked whether they are nullable, then associated fields
     *
     * @param $className
     * @param $fieldName
     * @return bool
     */
    private function isNullable($className, $fieldName)
    {
        $entityMetadata = $this->entityManager->getClassMetadata($className);

        return (in_array($fieldName, $entityMetadata->getFieldNames()) && $entityMetadata->isNullable($fieldName)) ||
            (in_array($fieldName, array_keys($entityMetadata->getAssociationMappings())) &&
                $entityMetadata->getAssociationMapping($fieldName)['joinColumns'][0]['nullable'] === true);
    }
}