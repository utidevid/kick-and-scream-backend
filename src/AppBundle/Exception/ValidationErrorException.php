<?php

namespace AppBundle\Exception;

class ValidationErrorException extends \Exception
{
    // Message shown to user
    protected $errorMessage;

    // Fields list with messages about each field
    protected $errorDetails;

    // HTTP response code
    protected $statusCode;

    // Alphanumeric error code to identify error
    protected $errorCode;

    public function __construct($errorMessage, $errorDetails = array(), $errorCode = null, $statusCode = 400)
    {
        parent::__construct($errorMessage);

        $this->setErrorDetails($errorDetails);
        $this->statusCode = $statusCode;
        $this->errorCode = $errorCode;
    }

    public function getErrorDetails()
    {
        return $this->errorDetails;
    }

    public function setErrorDetails($errorDetails)
    {
        $this->errorDetails = $errorDetails;
        return $this;
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function getErrorCode()
    {
        return $this->errorCode;
    }
}