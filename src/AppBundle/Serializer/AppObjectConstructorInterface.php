<?php

namespace AppBundle\Serializer;

use JMS\Serializer\Construction\ObjectConstructorInterface;

interface AppObjectConstructorInterface extends ObjectConstructorInterface
{
}