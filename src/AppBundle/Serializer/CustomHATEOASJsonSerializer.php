<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 9/28/2017
 * Time: 7:09 PM
 */

namespace AppBundle\Serializer;


use Hateoas\Model\Link;
use Hateoas\Serializer\JsonHalSerializer;
use JMS\Serializer\JsonSerializationVisitor;
use JMS\Serializer\SerializationContext;

/**
 * Is not being used in config.yml so far.
 *
 * Replaces HAL's _links.href format with just _links
 * Class CustomHATEOASJsonSerializer
 * @package AppBundle\Serializer1
 */
class CustomHATEOASJsonSerializer extends JsonHalSerializer
{

    /**
     * @param Link[] $links
     * @param JsonSerializationVisitor $visitor
     * @param SerializationContext $context
     */
    public function serializeLinks(array $links, JsonSerializationVisitor $visitor, SerializationContext $context)
    {
        $serializedLinks = array();

        foreach ($links as $link) {
            $serializedLinks[$link->getRel()] = $link->getHref();
        }

        $visitor->setData('_links', $serializedLinks);
    }
}