<?php

namespace AppBundle\Serializer;

use Doctrine\ORM\EntityNotFoundException;
use JMS\Serializer\Construction\DoctrineObjectConstructor;
use JMS\Serializer\Construction\UnserializeObjectConstructor;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\Metadata\ClassMetadata;
use JMS\Serializer\Metadata\PropertyMetadata;
use JMS\Serializer\VisitorInterface;
use RestBundle\Api\ApiProblem;
use RestBundle\Api\ApiProblemException;
use RestBundle\Factory\ApiProblemFactory;

class CustomObjectConstructor implements AppObjectConstructorInterface
{

    const IGNORE_RELATED_ENTITIES_METADATA = 'ignore_related_entities_metadata';

    /** @var  DoctrineObjectConstructor */
    private $doctrineConstructor;

    /** @var  UnserializeObjectConstructor */
    private $unserializeConstructor;
    /**
     * @var ApiProblemFactory
     */
    private $apiProblemFactory;

    public function __construct(DoctrineObjectConstructor $doctrineObjectConstructor, UnserializeObjectConstructor $unserializeObjectConstructor, ApiProblemFactory $apiProblemFactory)
    {
        $this->doctrineConstructor = $doctrineObjectConstructor;
        $this->unserializeConstructor = $unserializeObjectConstructor;
        $this->apiProblemFactory = $apiProblemFactory;
    }

    /**
     * Constructs a new object.
     *
     * Implementations could for example create a new object calling "new", use
     * "unserialize" techniques, reflection, or other means.
     *
     * @param VisitorInterface $visitor
     * @param ClassMetadata $metadata
     * @param mixed $data
     * @param array $type ["name" => string, "params" => array]
     * @param DeserializationContext $context
     *
     * @return object
     *
     */
    public function construct(VisitorInterface $visitor, ClassMetadata $metadata, $data, array $type, DeserializationContext $context)
    {
        $className = $metadata->name;

        if (preg_match('/(?:Bundle)\\\\Entity/', $className)) {
            $metadataStack = $context->getMetadataStack();

            // At the root
            if (!$metadataStack->offsetExists(1)) {
                if ($context instanceof IdAwareDeserializationContext && $context->getId()) {

                    $data['id'] = $context->getId();

                    $object = $this->doctrineConstructor->construct($visitor, $metadata, $data, $type, $context);

                    if ($object === null) {
                        throw new ApiProblemException(
                            $this->apiProblemFactory->createEntityNotFound($className, $data['id'])
                        );
                    }

                    return $object;
                } else if ($context instanceof ObjectAwareDeserializationContext && $context->getObject()) {
                    return $context->getObject();
                } else {
                    return new $className();
                }
            } else {
                $parent = $visitor->getResult();
                // Parent object property metadata that contain object being created
                /** @var PropertyMetadata $propertyMetadata */
                $propertyMetadata = $metadataStack->offsetGet(1);

                // If passed data is purely an integer, then presumably find an Entity by ID retrieved from data
                if (is_int($data) && preg_match('/(?:Bundle)\\\\Entity/', $type['name'])) {
                    $data = array('id' => $data);

                    // Remove groups from properties of related entities so they are not being parsed by visitor
                    $this->ignoreRelatedEntitiesMetadata($context, $metadata);

                    $object = $this->doctrineConstructor->construct($visitor, $metadata, $data, $type, $context);

                    if ($object === null) {
                        throw new ApiProblemException($this->apiProblemFactory->createRelatedEntityNotFound(
                            $propertyMetadata->class, $parent->getId(), $className, $data['id']));
                    }

                    return $object;
                }

                return null;
            }
        }

        return $this->unserializeConstructor->construct($visitor, $metadata, $data, $type, $context);
    }

    /**
     * Ignore related entities by excluding group from each entity to prevent visitor visiting
     * properties of an entity that is not being modified
     *
     * @param DeserializationContext $context
     * @param ClassMetadata $metadata
     */
    private function ignoreRelatedEntitiesMetadata(DeserializationContext $context, ClassMetadata $metadata)
    {
        if ($context->attributes->containsKey(self::IGNORE_RELATED_ENTITIES_METADATA) &&
            $context->attributes->get(self::IGNORE_RELATED_ENTITIES_METADATA)->get() === true) {
            foreach ($metadata->propertyMetadata as $key => $data) {
                $metadata->propertyMetadata[$key]->groups = [];
            }
        }
    }
}