<?php

namespace AppBundle\Serializer;

use JMS\Serializer\DeserializationContext;

class IdAwareDeserializationContext extends DeserializationContext
{
    protected $id;

    public function __construct($id)
    {
        $this->id = $id;

        parent::__construct();
    }

    public function getId()
    {
        return $this->id;
    }
}