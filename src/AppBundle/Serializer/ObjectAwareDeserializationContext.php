<?php

namespace AppBundle\Serializer;

use JMS\Serializer\DeserializationContext;

class ObjectAwareDeserializationContext extends DeserializationContext
{
    protected $object;

    public function __construct($object)
    {
        $this->object = $object;

        parent::__construct();
    }

    public function getObject()
    {
        return $this->object;
    }
}