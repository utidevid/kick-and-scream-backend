<?php

namespace AppBundle\Serializer;

use JMS\Serializer\Construction\ObjectConstructorInterface;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\Metadata\ClassMetadata;
use JMS\Serializer\VisitorInterface;

class ObjectConstructor implements ObjectConstructorInterface
{
    protected $constructors;

    public function __construct($constructors)
    {
        $this->constructors = $constructors;

        // Check if custom object constructor has a correct instance
        foreach ($this->constructors as $constructor) {
            if (!($constructor instanceof AppObjectConstructorInterface)) {
                throw new \Exception(get_class($constructor) . ' should be an instance of AppObjectConstructorInterface');
            }
        }
    }

    /**
     * Constructs a new object.
     *
     * Implementations could for example create a new object calling "new", use
     * "unserialize" techniques, reflection, or other means.
     *
     * @param VisitorInterface $visitor
     * @param ClassMetadata $metadata
     * @param mixed $data
     * @param array $type ["name" => string, "params" => array]
     * @param DeserializationContext $context
     *
     * @return object
     *
     * @throws \Exception
     */
    public function construct(VisitorInterface $visitor, ClassMetadata $metadata, $data, array $type, DeserializationContext $context)
    {
        foreach ($this->constructors as $constructor) {

            // if ($constructor->supportsClass($metadata->name)) { // Check if class constructor supports a class

            $obj = $constructor->construct($visitor, $metadata, $data, $type, $context);

            if ($obj !== false) {
                return $obj;
            }
        }

        throw new \Exception('None of the constructors has managed to created an object');
    }
}