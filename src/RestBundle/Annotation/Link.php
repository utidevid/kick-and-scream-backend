<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 9/25/2017
 * Time: 10:01 PM
 */

namespace RestBundle\Annotation;

/**
 * Class Link
 * @package RestBundle\Annotation
 *
 * @Annotation
 * @Target("CLASS")
 */
class Link
{
    /**
     * @Required
     */
    public $name;

    /**
     * @Required
     */
    public $route;
    public $params = array();
}