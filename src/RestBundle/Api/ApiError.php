<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 9/28/2017
 * Time: 8:54 PM
 */

namespace RestBundle\Api;


class ApiError
{
    private $fields = array();

    public function setError($fieldName, $error)
    {
        $this->fields[$fieldName][] = $error;
    }

    public function getErrors()
    {
        return $this->fields;
    }
}