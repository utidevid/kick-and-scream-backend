<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 9/24/2017
 * Time: 9:14 PM
 */

namespace RestBundle\Api;
use Doctrine\Common\Proxy\Exception\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ApiProblem
 * @package RestBundle\Api
 *
 * An adapter class that holds data for a application/problem+json content-type in the response
 */
class ApiProblem
{
    private $statusCode;
    private $type;
    private $title;
    private $extraData = array();

    const CONTENT_TYPE = 'application/problem+json';

    const TYPE_VALIDATION_ERROR = 'validation_error';
    const TYPE_INVALID_REQUEST_BODY_FORMAT = 'invalid_request_body_format';
    const INVALID_DATA_TYPE = 'invalid_data_type';
    const BAD_CREDENTIALS = 'bad_credentials';

    const TYPE_ABOUT_BLANK = 'about:blank';

    private static $titles = [
        self::TYPE_VALIDATION_ERROR => 'A validation error occurred',
        self::TYPE_INVALID_REQUEST_BODY_FORMAT => 'Invalid JSON content received',
        self::INVALID_DATA_TYPE => 'Invalid data type sent',
        self::BAD_CREDENTIALS => 'Wrong credentials received',
    ];

    /**
     * Get status code's title
     * @param $statusCode
     * @return null
     */
    public static function getStatusCodeTitle($statusCode)
    {
        return isset(Response::$statusTexts[$statusCode]) ? Response::$statusTexts[$statusCode] : null;
    }

    /**
     * ApiProblem constructor.
     * @param $statusCode - response status code
     * @param null $type - If status code is enough, the type can be set to null (about:blank) and title will be set to whatever is standard text for the status code is. Based on: https://tools.ietf.org/html/rfc7807  -- 4.2.  Predefined Problem Types
     */
    public function __construct($statusCode = 400, $type = null)
    {
        $this->statusCode = $statusCode;

        if ($type === null) {
            $this->type = self::TYPE_ABOUT_BLANK;
            $this->setTitleByStatusCode();
        } else {
            $this->initTitleByType($type);
        }
    }

    /**
     * @param $type
     */
    private function initTitleByType($type)
    {
        if (!isset(self::$titles[$type])) {
            throw new InvalidArgumentException('No title for a type ' . $this->type);
        }

        $this->type = $type;
        $this->title = self::$titles[$this->type];
    }

    private function setTitleByStatusCode()
    {
        $this->title = self::getStatusCodeTitle($this->statusCode) ?: 'Unknown Status Code';
    }

    public function createByStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        $this->setTitleByStatusCode();
    }

    public function createByType($type, $statusCode = 400)
    {
        $this->statusCode = $statusCode;
        $this->initTitleByType($type);
    }

    public function toArray()
    {
        return array_merge(
            [
                'status' => $this->statusCode,
                'type' => $this->type,
                'title' => $this->title
            ],
            $this->extraData
        );
    }



    public function setCustomType($type, $title)
    {
        $this->type = $type;
        $this->title = $title;
    }

    public function setDetail($detail)
    {
        $this->set('detail', $detail);
    }

    public function setErrors($errors)
    {
        $this->set('errors', $errors);
    }

    public function set($key, $value)
    {
        $this->extraData[$key] = $value;
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function getTitle()
    {
        return $this->title;
    }
}