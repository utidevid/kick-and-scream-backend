<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 01-Nov-17
 * Time: 3:18 PM
 */

namespace RestBundle\Builder;

abstract class AbstractCategoryDirector extends AbstractEntityDirector
{
    abstract public function __construct(CategoryBuilderInterface $builder);

    protected function generateUniqueTitle()
    {
        return 'Cat ' . $this->generateUniqueObjectId();
    }

    protected function generateUniqueSku()
    {
        return 'cat_' . $this->generateUniqueObjectId();
    }
}