<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 01-Nov-17
 * Time: 3:18 PM
 */

namespace RestBundle\Builder;


use RestBundle\Entity\Category;
use RestBundle\Entity\GameUser;

abstract class AbstractChallengeDirector extends AbstractEntityDirector
{
    abstract public function __construct(ChallengeBuilderInterface $builder, Category $category, GameUser $gameUser);

    protected function generateUniqueTitle()
    {
        return 'Title ' . $this->generateUniqueObjectId();
    }
}