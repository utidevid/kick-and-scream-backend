<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 01-Nov-17
 * Time: 3:18 PM
 */

namespace RestBundle\Builder;


use RestBundle\Entity\Category;
use RestBundle\Entity\GameUser;

abstract class AbstractEntityDirector
{
    /**
     * @return $this
     */
    abstract public function buildEntity();
    abstract public function getEntity();

    public function generateUniqueObjectId()
    {
        return substr(md5(uniqid(mt_rand(), true)), 0, 10);
    }
}