<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 01-Nov-17
 * Time: 5:56 PM
 */

namespace RestBundle\Builder;

abstract class AbstractGameUserDirector extends AbstractEntityDirector
{
    abstract public function __construct(GameUserBuilderInterface $builder);

    public function generateUniqueEmail()
    {
        return 'tempuser' . $this->generateUniqueObjectId() . '@tempemail.dev';
    }

    public function generateUniqueDeviceId()
    {
        return md5($this->generateUniqueObjectId());
    }
}