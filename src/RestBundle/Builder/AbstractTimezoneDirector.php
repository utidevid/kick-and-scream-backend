<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 01-Nov-17
 * Time: 3:18 PM
 */

namespace RestBundle\Builder;

abstract class AbstractTimezoneDirector extends AbstractEntityDirector
{
    abstract public function __construct(TimezoneBuilderInterface $builder);

    /**
     * Get random coordinates
     * @return string
     */
    protected function getRandomCoordinates()
    {
        return '+' . rand(1111,4444) .'−' . rand(1111,4444);
    }
}