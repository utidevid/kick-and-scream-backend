<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 01-Nov-17
 * Time: 4:59 PM
 */

namespace RestBundle\Builder;


use RestBundle\Entity\Category;

class CategoryBuilder implements CategoryBuilderInterface
{
    /**
     * @var Category
     */
    protected $category;

    public function __construct()
    {
        $this->category = new Category();
    }

    public function getEntity()
    {
        return $this->category;
    }

    public function setTitle($title)
    {
        $this->category->setTitle($title);
    }

    public function setLanguage($lang)
    {
        $this->category->setLanguage($lang);
    }

    public function setSku($sku)
    {
        $this->category->setSku($sku);
    }

    public function setObjectId($objectId)
    {
        $this->category->setObjectId($objectId);
    }
}