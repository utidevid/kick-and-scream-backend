<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 01-Nov-17
 * Time: 3:52 PM
 */

namespace RestBundle\Builder;


use RestBundle\Entity\Category;
use RestBundle\Entity\Challenge;
use RestBundle\Entity\GameUser;

interface CategoryBuilderInterface extends EntityBuilderInterface
{
    public function setTitle($title);
    public function setLanguage($lang);
    public function setSku($sku);
    public function setObjectId($objectId);
}