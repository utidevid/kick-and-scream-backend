<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 01-Nov-17
 * Time: 3:20 PM
 */

namespace RestBundle\Builder;


use RestBundle\Entity\Category;
use RestBundle\Entity\Challenge;
use RestBundle\Entity\GameUser;

class ChallengeBuilder implements ChallengeBuilderInterface
{
    /**
     * @var Challenge
     */
    protected $challenge;

    public function __construct()
    {
        $this->challenge = new Challenge();
    }

    public function setTitle($title)
    {
        $this->challenge->setTitle($title);
    }

    public function setContent($content)
    {
        $this->challenge->setContent($content);
    }

    public function setPoints($points)
    {
        $this->challenge->setPoints($points);
    }

    public function setGender($gender)
    {
        $this->challenge->setGender($gender);
    }

    public function setCode($code)
    {
        $this->challenge->setCode($code);
    }

    public function setType($type)
    {
        $this->challenge->setType($type);
    }

    public function setSource($source)
    {
        $this->challenge->setSource($source);
    }

    public function setModerated($moderated)
    {
        $this->challenge->setModerated($moderated);
    }

    public function setMinPlayersCount($count)
    {
        $this->challenge->setMinPlayersCount($count);
    }

    public function setCategory(Category $category)
    {
        $this->challenge->setCategory($category);
    }

    public function setGameUser(GameUser $gameUser)
    {
        $this->challenge->setGameUser($gameUser);
    }

    public function setRating($rating)
    {
        $this->challenge->setRating($rating);
    }

    public function setObjectId($objectId)
    {
        $this->challenge->setObjectId($objectId);
    }

    /**
     * @return Challenge
     */
    public function getEntity()
    {
        return $this->challenge;
    }
}