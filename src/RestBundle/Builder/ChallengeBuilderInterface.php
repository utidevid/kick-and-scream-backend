<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 01-Nov-17
 * Time: 3:52 PM
 */

namespace RestBundle\Builder;


use RestBundle\Entity\Category;
use RestBundle\Entity\GameUser;

interface ChallengeBuilderInterface extends EntityBuilderInterface
{
    public function setTitle($title);
    public function setContent($content);
    public function setPoints($points);
    public function setGender($gender);
    public function setCode($code);
    public function setType($type);
    public function setSource($source);
    public function setModerated($moderated);
    public function setMinPlayersCount($count);
    public function setRating($rating);
    public function setCategory(Category $category);
    public function setGameUser(GameUser $gameUser);
    public function setObjectId($objectId);
}