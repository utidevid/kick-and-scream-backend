<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 01-Nov-17
 * Time: 4:49 PM
 */

namespace RestBundle\Builder;


interface EntityBuilderInterface
{
    public function getEntity();
}