<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 01-Nov-17
 * Time: 5:48 PM
 */

namespace RestBundle\Builder;


use RestBundle\Entity\GameUser;

class GameUserBuilder implements GameUserBuilderInterface
{
    /**
     * @var GameUser
     */
    protected $gameUser;

    public function __construct()
    {
        $this->gameUser = new GameUser();
    }

    public function getEntity()
    {
        return $this->gameUser;
    }

    public function setPrimaryEmail($primaryEmail)
    {
        $this->gameUser->setPrimaryEmail($primaryEmail);
    }

    public function setDeviceName($deviceName)
    {
        $this->gameUser->setDeviceName($deviceName);
    }

    public function setDeviceId($deviceId)
    {
        $this->gameUser->setDeviceId($deviceId);
    }

    public function setRunCount($count)
    {
        $this->gameUser->setRunCount($count);
    }

    public function setGamePlayedCount($count)
    {
        $this->gameUser->setGamePlayedCount($count);
    }

    public function setLastGamePlayed($datetime)
    {
        $this->gameUser->setLastGamePlayed($datetime);
    }

    public function setLastLoggedIn($datetime)
    {
        $this->gameUser->setLastLoggedIn($datetime);
    }

    public function setLocale($locale)
    {
        $this->gameUser->setLocale($locale);
    }

    public function setTimezone($timezone)
    {
        $this->gameUser->setTimezone($timezone);
    }

    public function setPossibleAccounts(array $possibleAccounts)
    {
        $this->gameUser->setPossibleAccounts($possibleAccounts);
    }

    public function setObjectId($objectId)
    {
        $this->gameUser->setObjectId($objectId);
    }
}