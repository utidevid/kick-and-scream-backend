<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 01-Nov-17
 * Time: 5:43 PM
 */

namespace RestBundle\Builder;


interface GameUserBuilderInterface extends EntityBuilderInterface
{
    public function setPrimaryEmail($primaryEmail);
    public function setDeviceName($deviceName);
    public function setDeviceId($deviceId);
    public function setRunCount($count);
    public function setGamePlayedCount($count);
    public function setLastGamePlayed($datetime);
    public function setLastLoggedIn($datetime);
    public function setLocale($locale);
    public function setTimezone($timezone);
    public function setPossibleAccounts(array $possibleAccounts);
    public function setObjectId($objectId);

}