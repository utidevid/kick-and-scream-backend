<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 01-Nov-17
 * Time: 3:20 PM
 */

namespace RestBundle\Builder;

class TestCategoryDirector extends AbstractCategoryDirector
{
    /**
     * @var CategoryBuilderInterface
     */
    protected $builder;

    public function __construct(CategoryBuilderInterface $builder)
    {
        $this->builder = $builder;
    }

    /**
     * @return $this
     */
    public function buildEntity()
    {
        $this->builder->setObjectId($this->generateUniqueObjectId());
        $this->builder->setTitle($this->generateUniqueTitle());
        $this->builder->setLanguage('en');
        $this->builder->setSku($this->generateUniqueSku());

        return $this;
    }

    public function getEntity()
    {
        return $this->builder->getEntity();
    }
}