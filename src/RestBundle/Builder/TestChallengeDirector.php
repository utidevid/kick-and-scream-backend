<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 01-Nov-17
 * Time: 3:20 PM
 */

namespace RestBundle\Builder;


use RestBundle\Entity\Category;
use RestBundle\Entity\Challenge;
use RestBundle\Entity\GameUser;

class TestChallengeDirector extends AbstractChallengeDirector
{
    /**
     * @var ChallengeBuilderInterface
     */
    protected $builder;

    /**
     * @var Category
     */
    protected $category;

    /**
     * @var GameUser
     */
    protected $gameUser;

    protected $index;

    public function __construct(ChallengeBuilderInterface $builder, Category $category, GameUser $gameUser)
    {
        $this->builder = $builder;
        $this->category = $category;
        $this->gameUser = $gameUser;
    }

    /**
     * @return $this
     */
    public function buildEntity()
    {
        $this->builder->setTitle($this->generateUniqueTitle());
        $this->builder->setContent('This is a very impressive content.');
        $this->builder->setPoints(rand(1,15));
        $this->builder->setGender(Challenge::GENDER_UNISEX);
        $this->builder->setCode(Challenge::CODE_BASIC);
        $this->builder->setType(Challenge::TYPE_KICK);
        $this->builder->setSource(Challenge::SOURCE_GENERIC);
        $this->builder->setModerated(Challenge::MODERATED);
        $this->builder->setMinPlayersCount(2);
        $this->builder->setCategory($this->category);
        $this->builder->setGameUser($this->gameUser);
        $this->builder->setRating(rand(0,20));
        $this->builder->setObjectId($this->generateUniqueObjectId());

        return $this;
    }

    public function getEntity()
    {
        return $this->builder->getEntity();
    }
}