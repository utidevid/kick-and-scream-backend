<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 01-Nov-17
 * Time: 6:00 PM
 */

namespace RestBundle\Builder;

class TestGameUserDirector extends AbstractGameUserDirector
{
    /**
     * @var GameUserBuilderInterface
     */
    protected $builder;

    public function __construct(GameUserBuilderInterface $builder)
    {
        $this->builder = $builder;
    }

    /**
     * @return $this
     */
    public function buildEntity()
    {
        $this->builder->setPrimaryEmail($this->generateUniqueEmail());
        $this->builder->setDeviceId($this->generateUniqueDeviceId());
        $this->builder->setDeviceName('Test Phone');
        $this->builder->setObjectId($this->generateUniqueObjectId());

        return $this;
    }

    public function getEntity()
    {
        return $this->builder->getEntity();
    }
}