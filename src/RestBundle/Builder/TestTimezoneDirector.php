<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 01-Nov-17
 * Time: 3:20 PM
 */

namespace RestBundle\Builder;

class TestTimezoneDirector extends AbstractTimezoneDirector
{
    /**
     * @var TimezoneBuilderInterface
     */
    protected $builder;


    /**
     * @return $this
     */
    public function buildEntity()
    {
        $this->builder->setCc('US');
        $this->builder->setCoordinates('+2308−08222');
        $this->builder->setTimezone('America/Havana' . $this->generateUniqueObjectId());
        $this->builder->setComments('Random comments');
        $this->builder->setFormat('Canonical');
        $this->builder->setUtcOffset('−05:00');
        $this->builder->setUtcDstOffset('-04:00');
        $this->builder->setNotes('Random Notes');

        return $this;
    }

    public function getEntity()
    {
        return $this->builder->getEntity();
    }

    public function __construct(TimezoneBuilderInterface $builder)
    {
        $this->builder = $builder;
    }
}