<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 01-Nov-17
 * Time: 4:59 PM
 */

namespace RestBundle\Builder;


use RestBundle\Entity\TimeZone;

class TimezoneBuilder implements TimezoneBuilderInterface
{
    /**
     * @var TimeZone $timezone
     */
    protected $timezone;

    public function __construct()
    {
        $this->timezone = new TimeZone();
    }

    public function setCc($cc)
    {
        $this->timezone->setCc($cc);
    }

    public function setCoordinates($coordinates)
    {
        $this->timezone->setCoordinates($coordinates);
    }

    public function setTimezone($timezone)
    {
        $this->timezone->setTimezone($timezone);
    }

    public function setComments($comments)
    {
        $this->timezone->setComments($comments);
    }

    public function setFormat($format)
    {
        $this->timezone->setFormat($format);
    }

    public function setUtcOffset($utcOffset)
    {
        $this->timezone->setUtcOffset($utcOffset);
    }

    public function setUtcDstOffset($utcDstOffset)
    {
        $this->timezone->setUtcDstOffset($utcDstOffset);
    }

    public function setNotes($notes)
    {
        $this->timezone->setNotes($notes);
    }

    public function getEntity()
    {
        return $this->timezone;
    }
}