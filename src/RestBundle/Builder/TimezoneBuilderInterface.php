<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 01-Nov-17
 * Time: 3:52 PM
 */

namespace RestBundle\Builder;


use RestBundle\Entity\Category;
use RestBundle\Entity\Challenge;
use RestBundle\Entity\GameUser;

interface TimezoneBuilderInterface extends EntityBuilderInterface
{
    public function setCc($cc);
    public function setCoordinates($coordinates);
    public function setTimezone($timezone);
    public function setComments($comments);
    public function setFormat($format);
    public function setUtcOffset($utcOffset);
    public function setUtcDstOffset($utcDstOffset);
    public function setNotes($notes);
}