<?php

namespace RestBundle\Controller;

use AppBundle\Exception\ValidationErrorException;
use AppBundle\Serializer\CustomObjectConstructor;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\FOSRestController;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\Exception\RuntimeException;
use JMS\Serializer\SerializationContext;
use RestBundle\Api\ApiProblem;
use RestBundle\Api\ApiProblemException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BaseApiController extends FOSRestController
{
    protected $entity;

    protected function deserializeData(Request $request, $entityClass, DeserializationContext $context)
    {
        $context->setAttribute(CustomObjectConstructor::IGNORE_RELATED_ENTITIES_METADATA, true);

        $this->entity = $this->get('jms_serializer')
            ->deserialize(
                $request->getContent(),
                $entityClass,
                'json',
                $context
            );

        return $this->entity;
    }

    protected function validate($entity)
    {
        $errors = $this->get('validator')->validate($entity);

        $errorDetails= [];

        foreach ($errors as $error) {
            $field = $error->getPropertyPath();

            if (!array_key_exists($field, $errorDetails)) {
                $errorDetails[$field] = [];
            }
            $errorDetails[$field][] = $error->getMessage();
        }

        if (count($errorDetails) > 0) {
            $apiProblem = new ApiProblem(
                400,
                ApiProblem::TYPE_VALIDATION_ERROR
            );
            $apiProblem->setDetail('Submitted data are invalid. Please review them and try again.');
            $apiProblem->setErrors($errorDetails);

            throw new ApiProblemException($apiProblem);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function view($data = null, $statusCode = 200, array $headers = [])
    {
        $request = $this->get('request_stack')->getCurrentRequest();
        $context = new Context();
        $view = parent::view($data, $statusCode, $headers);

        $groups = array('read');
        if ($request->query->get('deep')) {
            $groups[] = 'deep';
        } else {
            $groups[] = 'relationship';
        }

        $context->setGroups($groups);
        $view->setContext($context);

        if (!isset($headers['Content-Type'])) {
            $view->setHeader('Content-Type', 'application/hal+json');
        }

        return $view;
    }
}
