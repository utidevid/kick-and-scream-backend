<?php

namespace RestBundle\Controller;

use AppBundle\Serializer\IdAwareDeserializationContext;
use FOS\RestBundle\View\View;
use JMS\Serializer\DeserializationContext;
use RestBundle\Create\JsonResponseCreator;
use RestBundle\Creator\CategoryCreator;
use RestBundle\Entity\Category;
use Swagger\Annotations as SWG;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CategoryController extends BaseApiController
{
    /**
     * Get category by ID
     *
     * @param $id
     * @return View
     *
     * @Rest\Get("/{id}", requirements={"id":"\d+"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="The category has been successfully retrieved"
     * )
     */
    public function getAction($id)
    {
        $category = $this->getDoctrine()->getRepository('RestBundle:Category')->find($id);

        if ($category == null) {
            return $this->get(JsonResponseCreator::class)
                ->addEntityNotFoundProblem(Category::class, $id)
                ->create();
        }

        return $this->view($category);
    }

    /**
     * Get all categories
     *
     * @return View
     * @Rest\Get()
     *
     * @SWG\Response(
     *     response=200,
     *     description="All categories have been successfully retrieved"
     * )
     */
    public function listAction()
    {
        $em = $this->getDoctrine()->getRepository('RestBundle:Category');

        return $this->view($em->findAll());
    }

    /**
     * Create a category
     *
     * @param Request $request
     * @return View
     *
     * @Rest\Post()
     *
     * @SWG\Response(
     *     response=201,
     *     description="The category has been successfully created"
     * )
     */
    public function createAction(Request $request)
    {
        $category = $this->deserializeData(
            $request,
            Category::class,
            DeserializationContext::create()->setGroups(['create'])
        );

        $this->validate($category);

        $this->get(CategoryCreator::class)->create($category);

        return $this->view($category, Response::HTTP_CREATED);
    }

    /**
     * Update a category
     *
     * @Rest\Patch("/{id}", requirements={"id":"\d+"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="The category has been successfully updated"
     * )
     *
     * @param Request $request
     * @return View
     */
    public function updateAction(Request $request, $id)
    {
        $context = new IdAwareDeserializationContext($id);
        $context->setGroups(['update']);

        $category = $this->deserializeData($request, Category::class, $context);

        $this->validate($category);

        $this->get(CategoryCreator::class)->update($category);

        return $this->view($category);
    }

    /**
     * Delete a category
     *
     * @Rest\Delete("/{id}", requirements={"id":"\d+"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="The category has been successfully deleted"
     * )
     *
     * @param Category $category
     * @return View
     */
    public function deleteAction(Category $category)
    {
        $this->get(CategoryCreator::class)->delete($category);

        return $this->view(null, Response::HTTP_NO_CONTENT);
    }
}
