<?php

namespace RestBundle\Controller;

use AppBundle\Serializer\IdAwareDataReplacerDeserializationContext;
use AppBundle\Serializer\IdAwareDeserializationContext;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\View\View;
use JMS\Serializer\DeserializationContext;
use RestBundle\Api\ApiProblem;
use RestBundle\Creator\ChallengeCreator;
use RestBundle\Entity\Challenge;
use RestBundle\Factory\ApiProblemFactory;
use RestBundle\Factory\PaginationFactory;
use RestBundle\Factory\ResponseFactory;
use Symfony\Component\HttpFoundation\Request;
use Swagger\Annotations as SWG;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;

/**
 * Challenge controller.
 */
class ChallengeController extends BaseApiController
{

    /**
     * Get challenge by ID
     *
     * @param $id
     * @return View
     *
     * @Rest\Get("/{id}", requirements={"id":"\d+"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="The challenge has been successfully retrieved"
     * )
     */
    public function getAction($id)
    {
        $challenge = $this->getDoctrine()->getRepository('RestBundle:Challenge')->find($id);

        if ($challenge == null) {
            return $this->get(ResponseFactory::class)->createProblemResponse(
                $this->get(ApiProblemFactory::class)->createEntityNotFound(Challenge::class, $id)
            );
        }

        return $this->view($challenge);
    }

    /**
     * Get all challenges
     *
     * @param Request $request
     * @return View
     * @Rest\Get()
     *
     * @SWG\Response(
     *     response=200,
     *     description="All or filtered challenges have been successfully retrieved"
     * )
     */
    public function listAction(Request $request)
    {
        $filter = $request->query->get('filter');
        
        $qb = $this->getDoctrine()->getRepository('RestBundle:Challenge')
            ->findAllQueryBuilder($filter);
        
        $paginatedCollection = $this->get(PaginationFactory::class)
            ->createCollection($qb, $request, 'challenge_list');

        return $this->view($paginatedCollection->toArray());
    }

    /**
     * Create a challenge
     *
     * @param Request $request
     * @return View
     *
     * @Rest\Post()
     *
     * @SWG\Response(
     *     response=201,
     *     description="The challenge has been successfully created"
     * )
     */
    public function createAction(Request $request)
    {
        $challenge = $this->deserializeData(
            $request,
            Challenge::class,
            DeserializationContext::create()->setGroups(['create'])
        );

        $this->validate($challenge);

        $this->get(ChallengeCreator::class)->create($challenge);

        return $this->view($challenge, Response::HTTP_CREATED);
    }

    /**
     * Update a challenge
     *
     * @Rest\Patch("/{id}", requirements={"id":"\d+"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="The challenge has been successfully updated"
     * )
     *
     * @param Request $request
     * @return View
     */
    public function updateAction(Request $request, $id)
    {
        $context = new IdAwareDeserializationContext($id);
        $context->setGroups(['update']);

        $challenge = $this->deserializeData($request, Challenge::class, $context);

        $this->validate($challenge);

        $this->get(ChallengeCreator::class)->update($challenge);

        return $this->view($challenge);
    }

    /**
     * Replace a challenge
     *
     * @Rest\Put("/{id}", requirements={"id":"\d+"})
     * @Rest\View(
     *     serializerGroups={"read", "relationship"},
     *     statusCode=200
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="The challenge has been successfully updated"
     * )
     *
     * @param Request $request
     * @return View
     */
    public function replaceAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $challenge = $em->getRepository('RestBundle:Challenge')->find($id);

        $context = new DeserializationContext();
        $context->setGroups(['create']);

        if ($challenge) {
            $context = new IdAwareDataReplacerDeserializationContext($id);
            $context->setGroups(['update', 'replace']);
        }

        $challenge = $this->deserializeData($request, Challenge::class, $context);

        $this->validate($challenge);

        $isNew = get_class($context) == DeserializationContext::class;
        $this->get(ChallengeCreator::class)->replace($challenge, $isNew);

        return $this->view($challenge, $isNew ? Response::HTTP_CREATED : Response::HTTP_OK);
    }


    /**
     * Delete a challenge
     *
     * @Rest\Delete("/{id}", requirements={"id":"\d+"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="The challenge has been successfully deleted"
     * )
     *
     * @param Challenge $challenge
     * @return View
     */
    public function deleteAction(Challenge $challenge)
    {
        $this->get(ChallengeCreator::class)->delete($challenge);

        return $this->view(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Rate a challenge
     *
     * @Rest\Put("/{id}", requirements={"id":"\d+"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="The challenge has been successfully rated up"
     * )
     *
     * @param Request $request
     * @param $id
     * @param $rateAction
     * @return View
     */
    public function rateAction(Request $request, $id, $rateAction)
    {
        if ($rateAction && ($rateAction == 'up' || $rateAction == 'down')) {
            $context = new IdAwareDeserializationContext($id);
            $context->setGroups(['rating']);
            $context->setAttribute('rate_action', $rateAction);

            $challenge = $this->deserializeData($request, Challenge::class, $context);

            $this->get(ChallengeCreator::class)->update($challenge);

            $context = new Context();
            $context->setGroups(['rating']);

            return $this->view($challenge)->setContext($context);
        }

        $apiProblem = $this->get(ApiProblemFactory::class)
            ->createSimpleProblem('Please provide up or down key to rate the challenge');

        return $this->get(ResponseFactory::class)->createProblemResponse($apiProblem);
    }
}
