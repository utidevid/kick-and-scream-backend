<?php

namespace RestBundle\Controller;

use AppBundle\Serializer\IdAwareDeserializationContext;
use FOS\RestBundle\View\View;
use JMS\Serializer\DeserializationContext;
use RestBundle\Create\JsonResponseCreator;
use RestBundle\Creator\GameUserCreator;
use RestBundle\Entity\Category;
use RestBundle\Entity\GameUser;
use RestBundle\Factory\PaginationFactory;
use Swagger\Annotations as SWG;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GameUserController extends BaseApiController
{
    /**
     * Get an in-game user by ID
     *
     * @param $id
     * @return View
     *
     * @Rest\Get("/{id}", requirements={"id":"\d+"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="An in-game user has been successfully retrieved."
     * )
     */
    public function getAction($id)
    {
        $gameUser = $this->getDoctrine()->getRepository(GameUser::class)->find($id);

        if ($gameUser == null) {
            return $this->get(JsonResponseCreator::class)
                ->addEntityNotFoundProblem(GameUser::class, $id)
                ->create();
        }

        return $this->view($gameUser);
    }

    /**
     * Get all in-game users
     *
     * @param Request $request
     * @return View
     * @Rest\Get()
     *
     * @SWG\Response(
     *     response=200,
     *     description="All in-game users have been successfully retrieved."
     * )
     */
    public function listAction(Request $request)
    {
        $qb = $this->getDoctrine()->getManager()->getRepository(GameUser::class)->findAllQueryBuilder();

        $paginatedCollection = $this->get(PaginationFactory::class)->createCollection($qb, $request, 'gameuser_list');

        return $this->view($paginatedCollection->toArray());
    }

    /**
     * Create an in-game user
     *
     * @param Request $request
     * @return View
     *
     * @Rest\Post()
     *
     * @SWG\Response(
     *     response=201,
     *     description="An in-game user has been successfully created."
     * )
     */
    public function createAction(Request $request)
    {
        $gameUser = $this->deserializeData(
            $request,
            GameUser::class,
            DeserializationContext::create()->setGroups(['create'])
        );

        $this->validate($gameUser);

        $this->get(GameUserCreator::class)->create($gameUser);

        return $this->view($gameUser, Response::HTTP_CREATED);
    }

    /**
     * Update an in-game user
     *
     * @Rest\Patch("/{id}", requirements={"id":"\d+"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="An in-game user has been successfully updated."
     * )
     *
     * @param Request $request
     * @return View
     */
    public function updateAction(Request $request, $id)
    {
        $context = new IdAwareDeserializationContext($id);
        $context->setGroups(['update']);

        $gameUser = $this->deserializeData($request, GameUser::class, $context);

        $this->validate($gameUser);

        $this->get(GameUserCreator::class)->update($gameUser);

        return $this->view($gameUser);
    }

    /**
     * Delete an in-game user
     *
     * @Rest\Delete("/{id}", requirements={"id":"\d+"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="An in-game user has been successfully deleted."
     * )
     *
     * @param GameUser $gameUser
     * @return View
     */
    public function deleteAction(GameUser $gameUser)
    {
        $this->get(GameUserCreator::class)->delete($gameUser);

        return $this->view(null, Response::HTTP_NO_CONTENT);
    }
}
