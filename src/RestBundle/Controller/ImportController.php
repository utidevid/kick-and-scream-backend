<?php

namespace RestBundle\Controller;

use FOS\RestBundle\View\View;
use Psr\Log\LoggerInterface;
use RestBundle\Api\ApiProblem;
use RestBundle\Creator\CategoryCreator;
use RestBundle\Creator\ChallengeCreator;
use RestBundle\Creator\GameUserCreator;
use RestBundle\Factory\ApiProblemFactory;
use RestBundle\Factory\ResponseFactory;
use Swagger\Annotations as SWG;

class ImportController extends BaseApiController
{
    /**
     * Import data from the remote server
     *
     * @return View
     *
     * @SWG\Response(
     *     response=200,
     *     description="Data have been successfully imported"
     * )
     */
    public function challengesAction()
    {
        set_time_limit(60 * 3);

        // Order is important because of relationships established between entities

        // Imported game users
        $this->get(GameUserCreator::class)->importAll();

        // Imported categories
        $this->get(CategoryCreator::class)->importAll();

        // Imported challenges
        $this->get(ChallengeCreator::class)->importAll();

        return $this->view(['result' => 'ok']);
    }
}
