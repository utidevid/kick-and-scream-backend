<?php

namespace RestBundle\Controller;

use RestBundle\Api\ApiProblem;
use RestBundle\Api\ApiProblemException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TokenController extends BaseApiController
{
    public function newAction(Request $request)
    {
        $username = $request->getUser();

        $user = $this->getDoctrine()->getRepository('AppBundle:User')
            ->findByUsername($username);

        $apiProblem = new ApiProblem(Response::HTTP_UNAUTHORIZED, ApiProblem::BAD_CREDENTIALS);
        $apiProblem->setDetail('Cannot issue a token for the given credentials.');
        
        if (!$user) {

            throw new ApiProblemException($apiProblem);
        }

        $isValid = $this->get('security.password_encoder')
            ->isPasswordValid($user, $request->getPassword());

        if (!$isValid) {
            throw new ApiProblemException($apiProblem);
        }
        
        $token = $this->get('lexik_jwt_authentication.encoder')
            ->encode([
                'username' => $user->getUsername()
            ]);
        
        return $this->view([
            'token' => $token
        ], Response::HTTP_CREATED);
    }
}
