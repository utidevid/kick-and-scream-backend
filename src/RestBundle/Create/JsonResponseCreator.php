<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 16-Nov-17
 * Time: 11:02 PM
 */

namespace RestBundle\Create;


use RestBundle\Api\ApiProblem;
use RestBundle\Factory\ApiProblemFactory;
use Symfony\Component\HttpFoundation\JsonResponse;

class JsonResponseCreator implements ResponseCreator
{
    /**
     * @var ApiProblemFactory
     */
    private $apiProblemFactory;

    /**
     * @var ApiProblem
     */
    private $apiProblem;

    public function __construct(ApiProblemFactory $apiProblemFactory)
    {
        $this->apiProblemFactory = $apiProblemFactory;
    }

    /**
     * Add a problem: Given entity has not been found
     *
     * @param $entityName
     * @param $entityId
     * @return $this
     */
    public function addEntityNotFoundProblem($entityName, $entityId)
    {
        $this->apiProblem = $this->apiProblemFactory->createEntityNotFound($entityName, $entityId);

        return $this;
    }

    /**
     * @return JsonResponse
     */
    public function create()
    {
        $response = new JsonResponse(
            $this->apiProblem->toArray(),
            $this->apiProblem->getStatusCode()
        );

        $response->headers->set('Content-Type', ApiProblem::CONTENT_TYPE);

        return $response;
    }
}