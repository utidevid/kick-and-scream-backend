<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 16-Nov-17
 * Time: 11:11 PM
 */

namespace RestBundle\Create;


use Symfony\Component\HttpFoundation\Response;

interface ResponseCreator
{
    /**
     * @return Response
     */
    public function create();
}