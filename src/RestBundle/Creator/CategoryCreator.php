<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 20-Dec-17
 * Time: 4:38 PM
 */

namespace RestBundle\Creator;


use AppBundle\Creator\AbstractEntityCreator;
use Doctrine\ORM\EntityManager;
use Parse\ParseObject;
use RestBundle\Entity\Category;
use RestBundle\Service\AbstractDataImporter;

class CategoryCreator extends AbstractEntityCreator implements Importable
{
    /**
     * @var AbstractDataImporter
     */
    private $dataImporter;

    public function __construct(EntityManager $entityManager, AbstractDataImporter $dataImporter)
    {
        parent::__construct($entityManager);
        $this->dataImporter = $dataImporter;
    }

    public function create($entity)
    {
        return parent::create($entity);
    }

    public function update($entity)
    {
        return parent::update($entity);
    }

    public function delete($entity)
    {
        parent::delete($entity);
    }

    /**
     * Import all data from remote server
     * @return array
     */
    public function importAll()
    {
        $importedCategories = $this->dataImporter->import('category');

        $categories = array();

        foreach ($importedCategories as $importedCategory) {
            /** @var ParseObject $parseObject */
            $parseObject = $importedCategory;

            // Do not import a user that already exists
            $category = $this->entityManager->getRepository('RestBundle:Category')
                ->findOneBy(['objectId' => $parseObject->getObjectId()]);

            if ($category) continue;

            $category = new Category();
            try {
                $category->setObjectId($parseObject->getObjectId());
                $category->setTitle($parseObject->get('title'));
                $category->setLanguage($parseObject->get('language'));
                $category->setSku($parseObject->get('sku'));

                $categories[] = $category;
                $this->create($category);
            } catch (\Exception $e) {
                $this->logger->error('Parse Adapter could not get values by keys in some fields from Category collection');
            }
        }

        return $categories;
    }
}