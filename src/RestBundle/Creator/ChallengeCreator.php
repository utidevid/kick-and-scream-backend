<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 19-Dec-17
 * Time: 4:49 PM
 */

namespace RestBundle\Creator;


use AppBundle\Creator\AbstractEntityCreator;
use Doctrine\ORM\EntityManager;
use Parse\ParseObject;
use Parse\ParseUser;
use Psr\Log\LoggerInterface;
use RestBundle\Entity\Challenge;
use RestBundle\Service\AbstractDataImporter;

class ChallengeCreator extends AbstractEntityCreator implements Importable
{
    /**
     * @var AbstractDataImporter
     */
    private $dataImporter;
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(EntityManager $entityManager, AbstractDataImporter $dataImporter, LoggerInterface $logger)
    {
        parent::__construct($entityManager);
        $this->dataImporter = $dataImporter;
        $this->logger = $logger;
    }

    public function create($entity)
    {
        return parent::create($entity);
    }

    public function update($entity)
    {
        return parent::update($entity);
    }

    public function delete($entity)
    {
        parent::delete($entity);
    }

    public function replace($entity, $isNew)
    {
        if (!$isNew) {
            $this->update($entity);
        } else {
            $this->create($entity);
        }
    }

    /**
     * Import all data from remote server
     * @return array
     */
    public function importAll()
    {
        $importedChallenges = $this->dataImporter->import('challenges');

        $challenges = array();

        foreach ($importedChallenges as $importedChallenge) {
            /** @var ParseObject $parseObject */
            $parseObject = $importedChallenge;

            // Do not import a user that already exists
            $challenge = $this->entityManager->getRepository('RestBundle:Challenge')
                ->findOneBy(['objectId' => $parseObject->getObjectId()]);

            if ($challenge) continue;

            $challenge = new Challenge();

            try {
                $challenge->setObjectId($parseObject->getObjectId());
                $challenge->setTitle($parseObject->get('title'));
                $challenge->setContent($parseObject->get('content'));
                $challenge->setPoints($parseObject->get('points'));
                $challenge->setGender($parseObject->get('gender'));
                $challenge->setMinPlayersCount($parseObject->get('minimum_players_count'));
                $challenge->setCode($parseObject->get('code'));
                $challenge->setType($parseObject->get('type'));
                $challenge->setSource($parseObject->get('source'));
                $challenge->setRating($parseObject->get('rating'));
                $challenge->setModerated($parseObject->get('moderated'));
                $challenge->setNativeId($parseObject->get('nativeId'));

                $category = $this->entityManager->getRepository('RestBundle:Category')
                    ->findOneBy(['sku' => 'cat_' . $parseObject->get('category')]);
                $challenge->setCategory($category);

                /** @var ParseUser $parseGameUser */
                $parseGameUser = $parseObject->get('user_id');
                if ($parseGameUser) {
                    $gameUser = $this->entityManager->getRepository('RestBundle:GameUser')
                        ->findOneBy(['objectId' => $parseGameUser->getObjectId()]);
                    $challenge->setGameUser($gameUser);
                }

                $challenges[] = $challenge;
                $this->create($challenge);
            } catch (\Exception $e) {
                $this->logger->error('Parse Adapter could not get values by keys in some fields from Challenge collection. Failed at ' . $parseObject->getObjectId());
            }
        }

        return $challenges;
    }
}