<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 20-Dec-17
 * Time: 4:38 PM
 */

namespace RestBundle\Creator;


use AppBundle\Creator\AbstractEntityCreator;
use Doctrine\ORM\EntityManager;
use Parse\ParseUser;
use Psr\Log\LoggerInterface;
use RestBundle\Entity\GameUser;
use RestBundle\Service\AbstractDataImporter;

class GameUserCreator extends AbstractEntityCreator implements Importable
{
    /**
     * @var AbstractDataImporter
     */
    private $dataImporter;
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(EntityManager $entityManager, AbstractDataImporter $dataImporter, LoggerInterface $logger)
    {
        parent::__construct($entityManager);
        $this->dataImporter = $dataImporter;
        $this->logger = $logger;
    }

    public function create($entity)
    {
        return parent::create($entity);
    }

    public function update($entity)
    {
        return parent::update($entity);
    }

    public function delete($entity)
    {
        parent::delete($entity);
    }

    /**
     * Import all data from remote server
     * @return array
     */
    public function importAll()
    {
        $importedGameUsers = $this->dataImporter->import('_User');

        $gameUsers = array();

        foreach ($importedGameUsers as $importedGameUser) {
            /** @var ParseUser $parseUser */
            $parseUser = $importedGameUser;

            // Do not import a user that already exists
            $gameUser = $this->entityManager->getRepository('RestBundle:GameUser')
                ->findOneBy(['objectId' => $parseUser->getObjectId()]);

            if ($gameUser) continue;

            $gameUser = new GameUser();
            $gameUser->setObjectId($parseUser->getObjectId());
            try {
                $gameUser->setPrimaryEmail($parseUser->get('primaryEmail'));
                $gameUser->setDeviceName($parseUser->get('deviceName'));
                $gameUser->setDeviceId(($parseUser->get('deviceId')));
                $gameUser->setRunCount($parseUser->get('runCount'));
                $gameUser->setGamePlayedCount($parseUser->get('gamePlayedCount'));
                $gameUser->setLastGamePlayed($parseUser->get('lastGamePlayed'));
                $gameUser->setLastLoggedIn($parseUser->get('lastLoggedIn'));
                $gameUser->setLocale($parseUser->get('locale'));
                $gameUser->setCreatedAtValue($parseUser->getCreatedAt());
                $gameUser->setUpdatedAtValue($parseUser->getUpdatedAt());

                $timezone = $parseUser->get('timezone');
                if ($timezone) {
                    $timezone = $this->entityManager->getRepository('RestBundle:TimeZone')->findTimeZoneByFacebookTimeZoneId($timezone);
                    $gameUser->setTimezone($timezone);
                }

                $possibleAccounts = $parseUser->get('possibleAccounts');
                if ($possibleAccounts) {
                    $possibleAccounts = json_decode($possibleAccounts, true);
                    $gameUser->setPossibleAccounts($possibleAccounts);
                }

                $gameUsers[] = $gameUser;
                $this->create($gameUser);
            } catch (\Exception $e) {
                $this->logger->error('Parse Adapter could not get values by keys in some fields from Users collection');
            }
        }

        return $gameUsers;
    }
}