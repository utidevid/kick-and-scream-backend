<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 23-Feb-18
 * Time: 2:15 PM
 */

namespace RestBundle\Creator;


interface Importable
{
    /**
     * Import all data from remote server
     * @return array
     */
    public function importAll();
}