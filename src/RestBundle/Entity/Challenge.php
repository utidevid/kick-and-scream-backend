<?php

namespace RestBundle\Entity;

/**
 * Challenge
 */
class Challenge
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $objectId;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $content;

    /**
     * @var int
     */
    protected $points;

    /**
     * @var int
     */
    protected $gender;

    /**
     * @var int
     */
    protected $minPlayersCount;

    /**
     * @var int
     */
    protected $code;

    /**
     * @var int
     */
    protected $type;

    /**
     * @var int
     */
    protected $source;

    /**
     * @var int
     */
    protected $rating = 0;

    /**
     * @var GameUser
     */
    protected $gameUser;

    /**
     * @var bool
     */
    protected $moderated;

    /**
     * @var int
     */
    protected $nativeId;

    /**
     * @var Category
     */
    protected $category;

    /**
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * @var \DateTime
     */
    protected $updatedAt;

    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;
    const GENDER_UNISEX = 3;

    const SOURCE_GENERIC = 1;
    const SOURCE_PERSONAL = 2;
    const SOURCE_SOCIAL = 3;

    const TYPE_KICK = 1;
    const TYPE_SCREAM = 2;

    const CATEGORY_CRAZY = 1;
    const CATEGORY_ADULT = 2;
    const CATEGORY_IN_A_PUB = 3;
    const CATEGORY_OUT_IN_PUBLIC = 4;
    const CATEGORY_CRAZY_PLUS = 5;
    const CATEGORY_ADULT_PLUS = 7;
    const CATEGORY_SOCIAL = 127;

    const CODE_BASIC = 1;
    const CODE_SOMEONE_OF_THE_SAME_SEX = 2;
    const CODE_SOMEONE_OF_THE_OPPOSITE_SEX = 3;
    const CODE_ANY_PLAYER = 4;
    const CODE_MALE_AND_FEMALE = 5;
    const CODE_TWO_MALES = 6;
    const CODE_TWO_FEMALES = 7;

    const CODE_VARIABLE_FEMALE = 'f';
    const CODE_VARIABLE_MALE = 'm';
    const CODE_VARIABLE_PLAYER = 'p';
    const CODE_VARIABLE_PLAYER_ONE = "p1";
    const CODE_VARIABLE_PLAYER_TWO = "p2";

    const MODERATED = true;
    const UNMODERATED = false;

    /**
     * Get array of genders
     *
     * @return array
     */
    public static function getGenders()
    {
        return [
            self::GENDER_MALE,
            self::GENDER_FEMALE,
            self::GENDER_UNISEX
        ];
    }

    /**
     * Get array of sources
     *
     * @return array
     */
    public static function getSources()
    {
        return [
            self::SOURCE_GENERIC,
            self::SOURCE_PERSONAL,
            self::SOURCE_SOCIAL,
        ];
    }

    /**
     * Get array of challenge type
     *
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_KICK,
            self::TYPE_SCREAM,
        ];
    }

    /**
     * Get array of codes
     *
     * @return array
     */
    public static function getCodes()
    {
        return [
            self::CODE_BASIC,
            self::CODE_SOMEONE_OF_THE_SAME_SEX,
            self::CODE_SOMEONE_OF_THE_OPPOSITE_SEX,
            self::CODE_ANY_PLAYER,
            self::CODE_MALE_AND_FEMALE,
            self::CODE_TWO_MALES,
            self::CODE_TWO_FEMALES
        ];
    }

    /**
     * Todo: Should be retrieved from a database
     *
     * Get array of categories
     *
     * @return array
     */
    public static function getCategories()
    {
        return [
            self::CATEGORY_CRAZY,
            self::CATEGORY_ADULT,
            self::CATEGORY_IN_A_PUB,
            self::CATEGORY_OUT_IN_PUBLIC,
            self::CATEGORY_CRAZY_PLUS,
            self::CATEGORY_ADULT_PLUS,
            self::CATEGORY_SOCIAL
        ];
    }

    /**
     * Get array of code variables
     *
     * @return array
     */
    public static function getCodeVariables()
    {
        return [
            self::CODE_VARIABLE_FEMALE,
            self::CODE_VARIABLE_MALE,
            self::CODE_VARIABLE_PLAYER,
            self::CODE_VARIABLE_PLAYER_ONE,
            self::CODE_VARIABLE_PLAYER_TWO
        ];
    }

    public static function getModerationFlags()
    {
        return [
            self::MODERATED,
            self::UNMODERATED,
        ];
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set objectId
     *
     * @param string $objectId
     *
     * @return Challenge
     */
    public function setObjectId($objectId)
    {
        $this->objectId = $objectId;

        return $this;
    }

    /**
     * Get objectId
     *
     * @return string
     */
    public function getObjectId()
    {
        return $this->objectId;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Challenge
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Challenge
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set points
     *
     * @param integer $points
     *
     * @return Challenge
     */
    public function setPoints($points)
    {
        $this->points = $points;

        return $this;
    }

    /**
     * Get points
     *
     * @return int
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * Set gender
     *
     * @param integer $gender
     *
     * @return Challenge
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return int
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set minPlayersCount
     *
     * @param integer $minPlayersCount
     *
     * @return Challenge
     */
    public function setMinPlayersCount($minPlayersCount)
    {
        $this->minPlayersCount = $minPlayersCount;

        return $this;
    }

    /**
     * Get minPlayersCount
     *
     * @return int
     */
    public function getMinPlayersCount()
    {
        return $this->minPlayersCount;
    }

    /**
     * Set code
     *
     * @param integer $code
     *
     * @return Challenge
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Challenge
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set source
     *
     * @param integer $source
     *
     * @return Challenge
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source
     *
     * @return int
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set rating
     *
     * @param integer $rating
     *
     * @return Challenge
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Rate up the rating
     *
     * @param $rating
     */
    public function rateUp($rating)
    {
        $this->rating+= $rating;
    }

    /**
     * Rate down the rating
     * @param $rating
     */
    public function rateDown($rating)
    {
        $this->rating-= $rating;
    }

    /**
     * Get rating
     *
     * @return int
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set game user
     *
     * @param GameUser $gameUser
     *
     * @return Challenge
     */
    public function setGameUser($gameUser)
    {
        $this->gameUser = $gameUser;

        return $this;
    }

    /**
     * Get game user
     *
     * @return GameUser
     */
    public function getGameUser()
    {
        return $this->gameUser;
    }

    /**
     * Get Game User ID
     *
     * @return int
     */
    public function getGameUserId()
    {
        return $this->gameUser !== null ? $this->gameUser->getId() : null;
    }

    /**
     * Set moderated
     *
     * @param boolean $moderated
     *
     * @return Challenge
     */
    public function setModerated($moderated)
    {
        $this->moderated = $moderated;

        return $this;
    }

    /**
     * Get moderated
     *
     * @return bool
     */
    public function getModerated()
    {
        return $this->moderated;
    }

    /**
     * Set nativeId
     *
     * @param integer $nativeId
     *
     * @return Challenge
     */
    public function setNativeId($nativeId)
    {
        $this->nativeId = $nativeId;

        return $this;
    }

    /**
     * Get nativeId
     *
     * @return int
     */
    public function getNativeId()
    {
        return $this->nativeId;
    }

    /**
     * Set category
     *
     * @param Category $category
     *
     * @return Challenge
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Get Category ID
     * @return int
     */
    public function getCategoryId()
    {
        return $this->category !== null ? $this->category->getId() : null;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Challenge
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @return Challenge
     */
    public function setUpdatedAt()
    {
        $this->updatedAt = new \DateTime();

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}

