<?php

namespace RestBundle\Entity;

/**
 * FacebookTimeZone
 */
class FacebookTimeZone
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $fbId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var TimeZone
     */
    private $timeZone;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set $fbId
     *
     * @param integer $fbId
     *
     * @return FacebookTimeZone
     */
    public function setFbId($fbId)
    {
        $this->fbId = $fbId;

        return $this;
    }

    /**
     * Get $fbId
     *
     * @return int
     */
    public function getFbId()
    {
        return $this->fbId;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return FacebookTimeZone
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return TimeZone
     */
    public function getTimeZone()
    {
        return $this->timeZone;
    }
}

