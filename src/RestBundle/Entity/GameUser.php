<?php

namespace RestBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * GameUser
 */
class GameUser
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $objectId;

    /**
     * @var string
     */
    private $primaryEmail;

    /**
     * @var string
     */
    private $deviceName;

    /**
     * @var string
     */
    private $deviceId;

    /**
     * @var int
     */
    private $runCount;

    /**
     * @var int
     */
    private $gamePlayedCount;

    /**
     * @var \DateTime
     */
    private $lastGamePlayed;

    /**
     * @var \DateTime
     */
    private $lastLoggedIn;

    /**
     * @var string
     */
    private $locale;

    /**
     * @var TimeZone
     */
    private $timezone;

    /**
     * @var array
     */
    private $possibleAccounts;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var ArrayCollection
     */
    protected $challenges;

    /**
     * @var ArrayCollection
     */
    protected $socialProfiles;

    public function __construct()
    {
        $this->challenges = new ArrayCollection();
        $this->socialProfiles = new ArrayCollection();
    }

    /**
     * @return ArrayCollection|Challenge[]
     */
    public function getChallenges()
    {
        return $this->challenges;
    }

    /**
     * @return ArrayCollection|GameUserSocialProfile[]
     */
    public function getSocialProfiles()
    {
        return $this->socialProfiles;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }


    /**
     * Set objectId
     *
     * @param string $objectId
     *
     * @return GameUser
     */
    public function setObjectId($objectId)
    {
        $this->objectId = $objectId;

        return $this;
    }

    /**
     * Get objectId
     *
     * @return string
     */
    public function getObjectId()
    {
        return $this->objectId;
    }

    /**
     * Set primaryEmail
     *
     * @param string $primaryEmail
     *
     * @return GameUser
     */
    public function setPrimaryEmail($primaryEmail)
    {
        $this->primaryEmail = $primaryEmail;

        return $this;
    }

    /**
     * Get primaryEmail
     *
     * @return string
     */
    public function getPrimaryEmail()
    {
        return $this->primaryEmail;
    }

    /**
     * Set deviceName
     *
     * @param string $deviceName
     *
     * @return GameUser
     */
    public function setDeviceName($deviceName)
    {
        $this->deviceName = $deviceName;

        return $this;
    }

    /**
     * Get deviceName
     *
     * @return string
     */
    public function getDeviceName()
    {
        return $this->deviceName;
    }

    /**
     * Set deviceId
     *
     * @param string $deviceId
     *
     * @return GameUser
     */
    public function setDeviceId($deviceId)
    {
        $this->deviceId = $deviceId;

        return $this;
    }

    /**
     * Get deviceId
     *
     * @return string
     */
    public function getDeviceId()
    {
        return $this->deviceId;
    }

    /**
     * Set runCount
     *
     * @param integer $runCount
     *
     * @return GameUser
     */
    public function setRunCount($runCount)
    {
        $this->runCount = $runCount;

        return $this;
    }

    /**
     * Get runCount
     *
     * @return int
     */
    public function getRunCount()
    {
        return $this->runCount;
    }

    /**
     * Set gamePlayedCount
     *
     * @param integer $gamePlayedCount
     *
     * @return GameUser
     */
    public function setGamePlayedCount($gamePlayedCount)
    {
        $this->gamePlayedCount = $gamePlayedCount;

        return $this;
    }

    /**
     * Get gamePlayedCount
     *
     * @return int
     */
    public function getGamePlayedCount()
    {
        return $this->gamePlayedCount;
    }

    /**
     * Set lastGamePlayed
     *
     * @param \DateTime $lastGamePlayed
     *
     * @return GameUser
     */
    public function setLastGamePlayed($lastGamePlayed)
    {
        $this->lastGamePlayed = $lastGamePlayed;

        return $this;
    }

    /**
     * Get lastGamePlayed
     *
     * @return \DateTime
     */
    public function getLastGamePlayed()
    {
        return $this->lastGamePlayed;
    }

    /**
     * Set lastLoggedIn
     *
     * @param \DateTime $lastLoggedIn
     *
     * @return GameUser
     */
    public function setLastLoggedIn($lastLoggedIn)
    {
        $this->lastLoggedIn = $lastLoggedIn;

        return $this;
    }

    /**
     * Get lastLoggedIn
     *
     * @return \DateTime
     */
    public function getLastLoggedIn()
    {
        return $this->lastLoggedIn;
    }

    /**
     * Set locale
     *
     * @param string $locale
     *
     * @return GameUser
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * Get locale
     *
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Set timeZone
     *
     * @param TimeZone $timezone
     *
     * @return GameUser
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;

        return $this;
    }

    /**
     * Get timezone ID
     *
     * @return int|null
     */
    public function getTimezoneId()
    {
        return $this->timezone !== null ? $this->timezone->getId() : null;
    }

    /**
     * Get timezone
     *
     * @return TimeZone
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * Set possibleAccounts
     *
     * @param string $possibleAccounts
     *
     * @return GameUser
     */
    public function setPossibleAccountsJsonArray($possibleAccounts)
    {
        $this->possibleAccounts = json_encode($possibleAccounts);

        return $this;
    }

    /**
     * Set possibleAccounts
     *
     * @param array $possibleAccounts
     *
     * @return GameUser
     */
    public function setPossibleAccounts($possibleAccounts)
    {
        $this->possibleAccounts = $possibleAccounts;

        return $this;
    }

    /**
     * Get possible accounts array
     *
     * @return string
     */
    public function getPossibleAccountsJsonArray()
    {
        if ($this->possibleAccounts != null) {
            return json_decode((string) $this->possibleAccounts, true);
        }

        return null;
    }

    /**
     * Get possibleAccounts
     *
     * @return array
     */
    public function getPossibleAccounts()
    {
        return $this->possibleAccounts;
    }

    /**
     * Set createdAt
     *
     * @return GameUser
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();

        return $this;
    }

    /**
     * Set createdAt with a specific value
     *
     * @param \DateTime $date
     * @return $this
     */
    public function setCreatedAtValue(\DateTime $date)
    {
        $this->createdAt = $date;

        return $this;
    }

        /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @return GameUser
     */
    public function setUpdatedAt()
    {
        $this->updatedAt = new \DateTime();

        return $this;
    }

    /**
     * Set updatedAt with a specific value
     *
     * @param \DateTime $date
     * @return $this
     */
    public function setUpdatedAtValue(\DateTime $date)
    {
        $this->updatedAt = $date;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}