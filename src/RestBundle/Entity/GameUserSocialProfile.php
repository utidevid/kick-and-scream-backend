<?php

namespace RestBundle\Entity;

/**
 * GameUserSocialProfile
 */
class GameUserSocialProfile
{
    const SOCIAL_NETWORK_FACEBOOK = 1;

    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;


    /**
     * @var int
     */
    private $id;

    /**
     * @var array
     */
    private $fbAuthDataJson;

    /**
     * @var int
     */
    private $fbId;

    /**
     * @var string
     */
    private $fbEmail;

    /**
     * @var string
     */
    private $fbLink;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $gender;

    /**
     * @var int
     */
    private $socialNetwork;

    /**
     * @var GameUser
     */
    private $gameUser;

    public static function getGenders()
    {
        return [
            self::GENDER_MALE,
            self::GENDER_FEMALE
        ];
    }

    /**
     * @return GameUser
     */
    public function getGameUser()
    {
        return $this->gameUser;
    }

    /**
     * @param GameUser $gameUser
     */
    public function setGameUser($gameUser)
    {
        $this->gameUser = $gameUser;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fbAuthDataJson
     *
     * @param array $fbAuthDataJson
     *
     * @return GameUserSocialProfile
     */
    public function setFbAuthDataJson($fbAuthDataJson)
    {
        $this->fbAuthDataJson = $fbAuthDataJson;

        return $this;
    }

    /**
     * Get fbAuthDataJson
     *
     * @return array
     */
    public function getFbAuthDataJson()
    {
        return $this->fbAuthDataJson;
    }

    /**
     * Set fbId
     *
     * @param integer $fbId
     *
     * @return GameUserSocialProfile
     */
    public function setFbId($fbId)
    {
        $this->fbId = $fbId;

        return $this;
    }

    /**
     * Get fbId
     *
     * @return int
     */
    public function getFbId()
    {
        return $this->fbId;
    }

    /**
     * Set fbEmail
     *
     * @param string $fbEmail
     *
     * @return GameUserSocialProfile
     */
    public function setFbEmail($fbEmail)
    {
        $this->fbEmail = $fbEmail;

        return $this;
    }

    /**
     * Get fbEmail
     *
     * @return string
     */
    public function getFbEmail()
    {
        return $this->fbEmail;
    }

    /**
     * Set fbLink
     *
     * @param string $fbLink
     *
     * @return GameUserSocialProfile
     */
    public function setFbLink($fbLink)
    {
        $this->fbLink = $fbLink;

        return $this;
    }

    /**
     * Get fbLink
     *
     * @return string
     */
    public function getFbLink()
    {
        return $this->fbLink;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return GameUserSocialProfile
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set gender
     *
     * @param integer $gender
     *
     * @return GameUserSocialProfile
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return int
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set socialNetwork
     *
     * @param integer $socialNetwork
     *
     * @return GameUserSocialProfile
     */
    public function setSocialNetwork($socialNetwork)
    {
        $this->socialNetwork = $socialNetwork;

        return $this;
    }

    /**
     * Get socialNetwork
     *
     * @return int
     */
    public function getSocialNetwork()
    {
        return $this->socialNetwork;
    }
}