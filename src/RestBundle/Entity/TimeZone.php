<?php

namespace RestBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * TimeZone
 */
class TimeZone
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $cc;

    /**
     * @var string
     */
    private $coordinates;

    /**
     * @var string
     */
    private $timezone;

    /**
     * @var FacebookTimeZone
     */
    private $facebookTimeZone;

    /**
     * @var string
     */
    private $comments;

    /**
     * @var string
     */
    private $format;

    /**
     * @var string
     */
    private $utcOffset;

    /**
     * @var string
     */
    private $utcDstOffset;

    /**
     * @var string
     */
    private $notes;

    /**
     * @var ArrayCollection|GameUser
     */
    protected $gameUsers;

    public function __construct()
    {
        $this->gameUsers = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cc
     *
     * @param string $cc
     *
     * @return TimeZone
     */
    public function setCc($cc)
    {
        $this->cc = $cc;

        return $this;
    }

    /**
     * Get cc
     *
     * @return string
     */
    public function getCc()
    {
        return $this->cc;
    }

    /**
     * Set coordinates
     *
     * @param string $coordinates
     *
     * @return TimeZone
     */
    public function setCoordinates($coordinates)
    {
        $this->coordinates = $coordinates;

        return $this;
    }

    /**
     * Get coordinates
     *
     * @return string
     */
    public function getCoordinates()
    {
        return $this->coordinates;
    }

    /**
     * Set timezone
     *
     * @param string $timezone
     *
     * @return TimeZone
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;

        return $this;
    }

    /**
     * Get timezone
     *
     * @return string
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * Set comments
     *
     * @param string $comments
     *
     * @return TimeZone
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set format
     *
     * @param string $format
     *
     * @return TimeZone
     */
    public function setFormat($format)
    {
        $this->format = $format;

        return $this;
    }

    /**
     * Get format
     *
     * @return string
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * Set utcOffset
     *
     * @param string $utcOffset
     *
     * @return TimeZone
     */
    public function setUtcOffset($utcOffset)
    {
        $this->utcOffset = $utcOffset;

        return $this;
    }

    /**
     * Get utcOffset
     *
     * @return string
     */
    public function getUtcOffset()
    {
        return $this->utcOffset;
    }

    /**
     * Set utcDstOffset
     *
     * @param string $utcDstOffset
     *
     * @return TimeZone
     */
    public function setUtcDstOffset($utcDstOffset)
    {
        $this->utcDstOffset = $utcDstOffset;

        return $this;
    }

    /**
     * Get utcDstOffset
     *
     * @return string
     */
    public function getUtcDstOffset()
    {
        return $this->utcDstOffset;
    }

    /**
     * Set notes
     *
     * @param string $notes
     *
     * @return TimeZone
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * @return ArrayCollection|GameUser[]
     */
    public function getGameUsers()
    {
        return $this->gameUsers;
    }

    /**
     * Check if one timezone equals another one
     *
     * @param TimeZone $timeZone
     * @return bool
     */
    public function isEqual(TimeZone $timeZone)
    {
        return $this->timezone == $timeZone->getTimezone();
    }

    /**
     * @return FacebookTimeZone
     */
    public function getFacebookTimeZone()
    {
        return $this->facebookTimeZone;
    }

    /**
     * @param FacebookTimeZone $facebookTimeZone
     * @return TimeZone
     */
    public function setFacebookTimeZone(FacebookTimeZone $facebookTimeZone)
    {
        $this->facebookTimeZone = $facebookTimeZone;

        return $this;
    }
}

