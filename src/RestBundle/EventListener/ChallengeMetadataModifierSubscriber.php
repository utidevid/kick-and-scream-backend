<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 9/25/2017
 * Time: 7:58 PM
 */

namespace RestBundle\EventListener;

use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\PreDeserializeEvent;
use JMS\Serializer\Metadata\PropertyMetadata;
use RestBundle\Api\ApiProblemException;
use RestBundle\Factory\ApiProblemFactory;

class ChallengeMetadataModifierSubscriber implements EventSubscriberInterface
{
    /**
     * @var ApiProblemFactory
     */
    private $apiProblemFactory;

    public function __construct(ApiProblemFactory $apiProblemFactory)
    {
        $this->apiProblemFactory = $apiProblemFactory;
    }

    public static function getSubscribedEvents()
    {
        return array(
            array(
                'event' => 'serializer.pre_deserialize',
                'method' => 'onPreDeserialize',
                'format' => 'json'
            )
        );
    }

    public function onPreDeserialize(PreDeserializeEvent $event)
    {
        $className = $event->getType()['name'];

        if (preg_match('/(?:Bundle)\\\\Entity/', $className)) {

            $classReflection = new \ReflectionClass($className);
            $classShortName = $classReflection->getShortName();

            $classMetadata = $event->getContext()->getMetadataFactory()
                ->getMetadataForClass($className)->propertyMetadata;

            $data = $event->getData();

            if ($classShortName == 'Challenge') {
                // Rate challenge up or down
                if ($event->getContext()->attributes->containsKey('rate_action')) {
                    $ratingAction = $event->getContext()->attributes->get('rate_action')->get();

                    $rating = $data['rating'];

                    if ($rating <= 0 || $rating > 5) {
                        throw new ApiProblemException($this->apiProblemFactory->createSingleValidationError('rating',
                            'Rating should be greater than 0 and less or equal 5'));
                    }

                    /** @var PropertyMetadata $ratingPropertyMetadata */
                    $ratingPropertyMetadata = $classMetadata['rating'];

                    if ($ratingAction == 'up') {
                        $ratingPropertyMetadata->setter = 'rateUp';
                    } else {
                        $ratingPropertyMetadata->setter = 'rateDown';
                    }
                }
            }
        }
    }
}