<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 03-Jan-18
 * Time: 8:27 PM
 */

namespace RestBundle\EventListener;

use JMS\Serializer\GraphNavigator;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\JsonDeserializationVisitor;

class DateTimeEventSubscriber implements SubscribingHandlerInterface
{
    /**
     * Return format:
     *
     *      array(
     *          array(
     *              'direction' => GraphNavigator::DIRECTION_SERIALIZATION,
     *              'format' => 'json',
     *              'type' => 'DateTime',
     *              'method' => 'serializeDateTimeToJson',
     *          ),
     *      )
     *
     * The direction and method keys can be omitted.
     *
     * @return array
     */
    public static function getSubscribingMethods()
    {
        return [
            [
                'direction' => GraphNavigator::DIRECTION_DESERIALIZATION,
                'format' => 'json',
                'type' => 'DateTime',
                'method' => 'deserializeDateFromString',
            ],
        ];
    }

    /**
     * @param JsonDeserializationVisitor $visitor
     * @param int                        $value   Timestamp
     * @param array                      $type
     * @return \DateTime
     */
    public function deserializeDateFromString(JsonDeserializationVisitor $visitor, $value, array $type)
    {
        die(var_dump('sads'));
        if (isset($type['params']) && !is_bool($key = array_search('Y-m', $type['params']))) {
            $value .= '-01';
            $type['params'][$key] = 'Y-m-d';
        }

        return $this->dateHandler->deserializeDateTimeFromJson($visitor, $value, $type);
    }
}