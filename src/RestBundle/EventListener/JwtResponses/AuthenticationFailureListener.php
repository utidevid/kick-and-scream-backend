<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 9/26/2017
 * Time: 9:10 PM
 */

namespace RestBundle\EventListener\JwtResponses;


use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationFailureEvent;
use RestBundle\Api\ApiProblem;
use RestBundle\Factory\ResponseFactory;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * By default, the response in case of failed authentication is just a json containing a failure message and a 401 status code, but you can set a custom response.
 *
 * Class AuthenticationFailureListener
 * @package RestBundle\EventListener\JwtResponses
 */
class AuthenticationFailureListener
{
    /**
     * @var ResponseFactory
     */
    private $responseFactory;

    public function __construct(ResponseFactory $responseFactory)
    {
        $this->responseFactory = $responseFactory;
    }

    /**
     * @param AuthenticationFailureEvent $event
     */
    public function onAuthenticationFailureResponse(AuthenticationFailureEvent $event)
    {
        $apiProblem = new ApiProblem(Response::HTTP_UNAUTHORIZED, ApiProblem::BAD_CREDENTIALS);
        $apiProblem->setDetail('Bad credentials, please verify that your username/password are correctly set.');

        $response = $this->responseFactory->createProblemResponse($apiProblem);

        $event->setResponse($response);
    }

}