<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 9/26/2017
 * Time: 9:54 PM
 */

namespace RestBundle\EventListener\JwtResponses;

use FOS\UserBundle\Model\UserInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;

/**
 * By default, the authentication response is just a json containing the JWT but you can add your own public data to it.
 *
 * Class AuthenticationSuccessListener
 * @package RestBundle\EventListener\JwtResponses
 */
class AuthenticationSuccessListener
{
    /**
     * @param AuthenticationSuccessEvent $event
     */
    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event)
    {
        $data = $event->getData();
        $user = $event->getUser();

        if (!$user instanceof UserInterface) {
            return;
        }

        $event->setData($data);
    }
}