<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 9/26/2017
 * Time: 10:01 PM
 */

namespace RestBundle\EventListener\JwtResponses;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * By default the JWT payload will contain the username and the token TTL, but you can add your own data.
 * You can also modify the header to fit on your application context.
 *
 * Class JWTCreatedListener
 * @package RestBundle\EventListener\JwtResponses
 */
class JWTCreatedListener
{
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @param JWTCreatedEvent $event
     *
     * @return void
     */
    public function onJWTCreated(JWTCreatedEvent $event)
    {
        /*$request = $this->requestStack->getCurrentRequest();

        $payload = $event->getData();

        $payload['ip'] = $request->getClientIp();

        $event->setData($payload);

        $header        = $event->getHeader();
        $header['cty'] = 'JWT';

        $event->setHeader($header);*/
    }
}