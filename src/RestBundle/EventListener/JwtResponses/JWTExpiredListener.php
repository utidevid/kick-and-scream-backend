<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 9/26/2017
 * Time: 8:55 PM
 */

namespace RestBundle\EventListener\JwtResponses;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTExpiredEvent;
use RestBundle\Api\ApiProblem;
use RestBundle\Factory\ResponseFactory;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;

/**
 * Customizing the response message on expired token
 *
 * Class JWTExpiredListener
 * @package RestBundle\EventListener\JwtResponses
 */
class JWTExpiredListener
{
    /**
     * @var RouterInterface
     */
    private $router;
    /**
     * @var ResponseFactory
     */
    private $responseFactory;

    public function __construct(RouterInterface $router, ResponseFactory $responseFactory)
    {
        $this->router = $router;
        $this->responseFactory = $responseFactory;
    }

    /**
     * @param JWTExpiredEvent $event
     */
    public function onJWTExpired(JWTExpiredEvent $event)
    {
        $newTokenUri = $this->router->generate('api_new_token');

        $apiProblem = new ApiProblem(Response::HTTP_UNAUTHORIZED);
        $apiProblem->setDetail('Token has expired. Create a new one at ' . $newTokenUri);
        $apiProblem->set('_links', [
            'generate_new_token' => $newTokenUri
        ]);

        $response = $this->responseFactory->createProblemResponse($apiProblem);

        $event->setResponse($response);
    }
}