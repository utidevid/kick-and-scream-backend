<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 9/26/2017
 * Time: 8:48 PM
 */

namespace RestBundle\EventListener\JwtResponses;


use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTInvalidEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTNotFoundEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Response\JWTAuthenticationFailureResponse;
use RestBundle\Api\ApiProblem;
use RestBundle\Factory\ResponseFactory;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class JWTInvalidListener
 * @package RestBundle\EventListener\JwtResponses
 */
class JWTInvalidListener
{
    /**
     * @var RouterInterface
     */
    private $router;
    /**
     * @var ResponseFactory
     */
    private $responseFactory;

    public function __construct(RouterInterface $router, ResponseFactory $responseFactory)
    {
        $this->router = $router;
        $this->responseFactory = $responseFactory;
    }

    /**
     * By default, if the token is invalid, the response is just a json containing the corresponding error message and a 401 status code, but you can set a custom response.
     *
     * @param JWTInvalidEvent $event
     */
    public function onJWTInvalid(JWTInvalidEvent $event)
    {
        $newTokenUri = $this->router->generate('api_new_token');

        $apiProblem = new ApiProblem(Response::HTTP_UNAUTHORIZED);
        $apiProblem->setDetail('Your token is invalid, please generate a new one at ' . $newTokenUri);
        $apiProblem->set('_links', [
            'generate_new_token' => $newTokenUri
        ]);

        $response = $this->responseFactory->createProblemResponse($apiProblem);

        $event->setResponse($response);
    }

    /**
     * By default, if no token is found in a request, the authentication listener will either call the entry point
     * that returns a unauthorized (401) json response, or (if the firewall allows anonymous requests),
     * just let the request continue.
     *
     * Thanks to this event, you can set a custom response.
     *
     * @param JWTNotFoundEvent $event
     */
    public function onJWTNotFound(JWTNotFoundEvent $event)
    {
        $newTokenUri = $this->router->generate('api_new_token');

        $apiProblem = new ApiProblem(Response::HTTP_FORBIDDEN);
        $apiProblem->setDetail('Missing token. Create a new one at ' . $newTokenUri);
        $apiProblem->set('_links', [
            'generate_new_token' => $newTokenUri
        ]);

        $response = $this->responseFactory->createProblemResponse($apiProblem);

        $event->setResponse($response);
    }
}