<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 9/25/2017
 * Time: 7:58 PM
 */

namespace RestBundle\EventListener;


use Doctrine\Common\Annotations\Reader;
use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\ObjectEvent;
use JMS\Serializer\JsonSerializationVisitor;
use RestBundle\Annotation\Link;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\Routing\RouterInterface;

class LinkSerializationSubscriber implements EventSubscriberInterface
{
    /**
     * @var RouterInterface
     */
    private $router;
    /**
     * @var Reader
     */
    private $annotationsReader;
    /**
     * @var ExpressionLanguage
     */
    private $expressionLanguage;

    public function __construct(RouterInterface $router, Reader $annotationsReader, ExpressionLanguage $expressionLanguage)
    {
        $this->router = $router;
        $this->annotationsReader = $annotationsReader;
        $this->expressionLanguage = $expressionLanguage;
    }

    public static function getSubscribedEvents()
    {
        return array(
            array(
                'event' => 'serializer.post_serialize',
                'method' => 'onPostSerialize',
                'format' => 'json'
            )
        );
    }

    public function onPostSerialize(ObjectEvent $objectEvent)
    {
        /** @var JsonSerializationVisitor $visitor */
        $visitor = $objectEvent->getVisitor();

        $object = $objectEvent->getObject();
        $annotations = $this->annotationsReader->getClassAnnotations(new \ReflectionObject($object));
        $links = array();
        
        foreach ($annotations as $annotation) {
            if ($annotation instanceof Link) {
                $uri = $this->router->generate(
                    $annotation->route,
                    $this->resolveParams($annotation->params, $object)
                );

                $links[$annotation->name] = $uri;
            }
        }

        // Otherwise, JMS Serializer doesn't like when you override exiting properties, which is a _links in root
        if ($links) {
            $visitor->setData("_links", $links);
        }
    }

    private function resolveParams(array $params, $object)
    {
        foreach($params as $key => $param) {
            $params[$key] = $this->expressionLanguage
                ->evaluate($param, array('object' => $object));
        }

        return $params;
    }
}