<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 9/25/2017
 * Time: 1:37 AM
 */

namespace RestBundle\Factory;

use RestBundle\Api\ApiError;
use RestBundle\Api\ApiProblem;
use Symfony\Component\HttpFoundation\Response;

class ApiProblemFactory
{

    /**
     * @var ApiProblem
     */
    private $apiProblem;
    /**
     * @var ApiError
     */
    private $apiError;

    public function __construct(ApiProblem $apiProblem, ApiError $apiError)
    {
        $this->apiProblem = $apiProblem;
        $this->apiError = $apiError;
    }

    /**
     * Create simple API problem
     *
     * @param int $statusCode
     * @param $details
     * @return ApiProblem
     */
    public function createSimpleProblem($details, $statusCode = 400)
    {
        $this->apiProblem->createByStatusCode($statusCode);
        $this->apiProblem->setDetail($details);

        return $this->apiProblem;
    }

    /**
     * Create a single validation error (not multiple validation errors)
     *
     * @param $fieldName
     * @param $errorText
     * @return ApiProblem
     */
    public function createSingleValidationError($fieldName, $errorText)
    {
        $this->apiError->setError($fieldName, $errorText);

        $this->apiProblem->createByType(ApiProblem::TYPE_VALIDATION_ERROR);
        $this->apiProblem->setErrors($this->apiError->getErrors());

        return $this->apiProblem;
    }

    /**
     * Create an Entity Not Found API problem
     *
     * @param $entityName - e.g. Challenge or Category
     * @param $entityId
     * @return ApiProblem
     */
    public function createEntityNotFound($entityName, $entityId)
    {
        $entityReflection = new \ReflectionClass($entityName);
        $entityName = $entityReflection->getShortName();

        $this->apiProblem->createByStatusCode(Response::HTTP_NOT_FOUND);
        $this->apiProblem->setDetail(sprintf('A %s entity has not been found by "%s" ID',
            $entityName, $entityId));
        $this->apiProblem->set('entity', [
            'name' => $entityName,
            'id' => $entityId
        ]);

        return $this->apiProblem;
    }

    public function createRelatedEntityNotFound($parentEntityName, $parentEntityId, $relatedEntityName, $relatedEntityId)
    {
        $parentEntityReflection = new \ReflectionClass($parentEntityName);
        $relatedEntityReflection = new \ReflectionClass($relatedEntityName);
        $parentEntityName = $parentEntityReflection->getShortName();
        $relatedEntityName = $relatedEntityReflection->getShortName();

        $this->apiProblem->createByStatusCode(Response::HTTP_NOT_FOUND);
        $this->apiProblem->setDetail(sprintf('A %s sub entity has not been found by "%s" ID to link with %s entity',
            $relatedEntityName, $relatedEntityId, $parentEntityName));
        $this->apiProblem->set('sub_entity', [
            'name' => $relatedEntityName,
            'id' => $relatedEntityId
        ]);
        $this->apiProblem->set('entity', [
            'name' => $parentEntityName,
            'id' => $parentEntityId ?: 'new'
        ]);

        return $this->apiProblem;
    }
}