<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 01-Nov-17
 * Time: 4:16 PM
 */

namespace RestBundle\Factory;

use RestBundle\Builder\TestCategoryDirector;
use RestBundle\Entity\Category;

class CategoryFactory
{
    /**
     * @required
     * @param TestCategoryDirector $director
     * @return Category
     */
    public function createTestCategory(TestCategoryDirector $director)
    {
        return $director->buildEntity()->getEntity();
    }
}