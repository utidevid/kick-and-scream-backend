<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 01-Nov-17
 * Time: 4:16 PM
 */

namespace RestBundle\Factory;

use RestBundle\Builder\ChallengeBuilder;
use RestBundle\Builder\TestChallengeDirector;
use RestBundle\Entity\Category;
use RestBundle\Entity\Challenge;
use RestBundle\Entity\GameUser;

class ChallengeFactory
{
    /**
     * @required
     * @param TestChallengeDirector $director
     * @return Challenge
     */
    public function createTestChallenge(TestChallengeDirector $director)
    {
        return $director->buildEntity()->getEntity();
    }

    /**
     * @param Category $category
     * @param GameUser $gameUser
     * @return Challenge
     */
    public static function createChallenge(Category $category, GameUser $gameUser)
    {
        $director = new TestChallengeDirector(new ChallengeBuilder(), $category, $gameUser);
        return $director->buildEntity()->getEntity();
    }
}