<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 01-Nov-17
 * Time: 4:16 PM
 */

namespace RestBundle\Factory;

use RestBundle\Builder\TestGameUserDirector;
use RestBundle\Entity\GameUser;

class GameUserFactory
{
    /**
     * @required
     * @param TestGameUserDirector $director
     * @return GameUser
     */
    public function createTestGameUser(TestGameUserDirector $director)
    {
        return $director->buildEntity()->getEntity();
    }
}