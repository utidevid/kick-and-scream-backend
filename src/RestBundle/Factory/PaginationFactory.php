<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 9/25/2017
 * Time: 6:05 PM
 */

namespace RestBundle\Factory;


use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use RestBundle\Pagination\PaginatedCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;

class PaginationFactory
{
    const MAX_PER_PAGE = 30;

    /**
     * @var RouterInterface
     */
    private $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * Create a paginated collection
     *
     * @param QueryBuilder $qb
     * @param Request $request
     * @param $route
     * @param array $routeParams
     * @param int $maxPerPage
     * @return PaginatedCollection
     */
    public function createCollection(QueryBuilder $qb, Request $request, $route, array $routeParams = array(), $maxPerPage = self::MAX_PER_PAGE)
    {
        $page = $request->query->get('page', 1);

        $adapter = new DoctrineORMAdapter($qb);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage($maxPerPage);
        $pagerfanta->setCurrentPage($page);

        $items = array();

        foreach ($pagerfanta->getCurrentPageResults() as $item) {
            $items[] = $item;
        }

        $paginatedCollection = new PaginatedCollection($items, $pagerfanta->getNbResults());

        $routeParams = array_merge($routeParams, $request->query->all());
        $createLinkUrl = function($targetPage) use ($route, $routeParams) {
            return $this->router->generate($route, array_merge(
                $routeParams,
                ['page' => $targetPage]
            ));
        };

        $paginatedCollection->addLink('self', $createLinkUrl($page));

        $paginatedCollection->addLink('first', $createLinkUrl(1));
        if ($pagerfanta->hasPreviousPage()) {
            $paginatedCollection->addLink('prev', $createLinkUrl($pagerfanta->getPreviousPage()));
        }

        if ($pagerfanta->hasNextPage()) {
            $paginatedCollection->addLink('next', $createLinkUrl($pagerfanta->getNextPage()));
        }
        $paginatedCollection->addLink('last', $createLinkUrl($pagerfanta->getNbPages()));

        return $paginatedCollection;
    }
}