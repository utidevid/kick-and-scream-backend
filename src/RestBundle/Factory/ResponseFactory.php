<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 9/26/2017
 * Time: 11:00 PM
 */

namespace RestBundle\Factory;


use RestBundle\Api\ApiProblem;
use Symfony\Component\HttpFoundation\JsonResponse;

class ResponseFactory
{
    public function createProblemResponse(ApiProblem $apiProblem)
    {
        $response = new JsonResponse(
            $apiProblem->toArray(),
            $apiProblem->getStatusCode()
        );

        $response->headers->set('Content-Type', ApiProblem::CONTENT_TYPE);

        return $response;
    }
}