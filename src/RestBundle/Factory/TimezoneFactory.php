<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 01-Nov-17
 * Time: 4:16 PM
 */

namespace RestBundle\Factory;

use RestBundle\Builder\TestTimezoneDirector;
use RestBundle\Entity\Category;

class TimezoneFactory
{
    /**
     * @required
     * @param TestTimezoneDirector $director
     * @return Category
     */
    public function createTestTimezone(TestTimezoneDirector $director)
    {
        return $director->buildEntity()->getEntity();
    }
}