<?php

namespace RestBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChallengeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('object_id', TextType::class,
                [
                    'property_path' => 'objectId'
                ]
            )
            ->add('title')
            ->add('content')
            ->add('points')
            ->add('gender')
            ->add('min_players_count', NumberType::class,
                [
                    'property_path' => 'minPlayersCount'
                ]
            )
            ->add('code')
            ->add('type')
            ->add('source')
            ->add('rating')
            ->add('user_id', NumberType::class,
                [
                    'property_path' => 'userId'
                ]
            )
            ->add('moderated')
            ->add('native_id', NumberType::class,
                [
                    'property_path' => 'nativeId'
                ]
            )
            ->add('category_id', NumberType::class,
                [
                    'property_path' => 'categoryId'
                ])
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'RestBundle\Entity\Challenge',
            'csrf_protection' => false,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'restbundle_challenge';
    }


}
