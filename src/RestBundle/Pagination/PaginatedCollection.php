<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 9/25/2017
 * Time: 5:26 PM
 */

namespace RestBundle\Pagination;


class PaginatedCollection
{
    private $items;
    private $total;
    private $count;

    private $links = array();

    /**
     * PaginatedCollection constructor.
     * @param $items
     * @param $total
     */
    public function __construct(array $items, $total)
    {
        $this->items = $items;
        $this->total = $total;
        $this->count = count($items);
    }

    public function addLink($ref, $url)
    {
        $this->links[$ref] = $url;
    }

    public function toArray()
    {
        return array(
            'total' => $this->total,
            'count' => $this->count,
            '_links' => $this->links,
            'items' => $this->items,
        );
    }
}