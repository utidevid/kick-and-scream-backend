<?php

namespace RestBundle\Repository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use RestBundle\Entity\TimeZone;

/**
 * TimeZoneRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class TimeZoneRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * Get timezone by Facebook time zone ID through a one-to-one relationship
     *
     * @param $facebookTimeZoneId
     * @return TimeZone|null
     */
    public function findTimeZoneByFacebookTimeZoneId($facebookTimeZoneId)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder()
            ->select(array('tz'))
            ->from('RestBundle:TimeZone', 'tz')
            ->join('tz.facebookTimeZone', 'ftz')
            ->where('ftz.fbId = :fbId')
            ->setParameter('fbId', $facebookTimeZoneId);

        try {
            return $qb->getQuery()->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        } catch (NonUniqueResultException $e) {
            return null;
        }

    }
}
