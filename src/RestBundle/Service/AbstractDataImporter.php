<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 23-Feb-18
 * Time: 2:20 PM
 */

namespace RestBundle\Service;


abstract class AbstractDataImporter
{

    /**
     * @var RemoteServerAdapterInterface
     */
    protected $remoteServer;

    public function __construct(RemoteServerAdapterInterface $remoteServer)
    {
        $this->remoteServer = $remoteServer;
    }

    /**
     * Return imported data from the remote server
     *
     * @param $collection
     * @return array
     */
    abstract public function import($collection);
}