<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 23-Feb-18
 * Time: 1:56 PM
 */

namespace RestBundle\Service;


class DataImporter extends AbstractDataImporter
{
    /**
     * Return imported data from the remote server
     *
     * @param $collection
     * @return array
     */
    public function import($collection)
    {
        return $this->remoteServer->query($collection)->findAll();
    }
}