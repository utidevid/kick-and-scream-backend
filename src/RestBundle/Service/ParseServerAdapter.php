<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 23-Feb-18
 * Time: 12:01 PM
 */

namespace RestBundle\Service;


use Monolog\Logger;
use Parse\HttpClients\ParseCurlHttpClient;
use Parse\ParseClient;
use Parse\ParseQuery;
use Psr\Log\LoggerInterface;

class ParseServerAdapter implements RemoteServerAdapterInterface
{
    /**
     * @var ParseQuery
     */
    protected $parseQuery;

    /**
     * @var array
     */
    private $data = array();
    /**
     * @var Logger
     */
    private $logger;

    public function __construct($appId, $restKey, $masterKey, $parseServerUrl, $mountPath = '/', LoggerInterface $logger)
    {
        $this->logger = $logger;

        try {
            ParseClient::initialize($appId, $restKey, $masterKey);
            ParseClient::setServerURL($parseServerUrl, $mountPath);
        } catch (\Exception $e) {
            $logger->error('Parse client could not be either initialized or set to a remote server');
        }

        // Set HTTP Client
        ParseClient::setHttpClient(new ParseCurlHttpClient());
    }

    /**
     * Check server health
     *
     * @return bool
     */
    public function isAlive()
    {
        $health = ParseClient::getServerHealth();

        if($health['status'] === 200) {
            return true;
        }

        return false;
    }

    public function query($className)
    {
        $this->parseQuery = new ParseQuery($className);
        return $this;
    }

    public function findAll()
    {
        $this->parseQuery->limit(1000);

        $output = $this->parseQuery->find();

        if ($output) {
            $this->data = array_merge($this->data, $output);
            $this->parseQuery->skip(count($this->data));

            return $this->findAll();
        }

        return $this->data;
    }
}