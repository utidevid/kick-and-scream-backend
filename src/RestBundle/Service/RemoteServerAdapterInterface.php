<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 23-Feb-18
 * Time: 1:56 PM
 */

namespace RestBundle\Service;


interface RemoteServerAdapterInterface
{
    /**
     * Start querying on a collection of data
     *
     * @param $className
     * @return $this
     */
    public function query($className);
    /**
     * Retrieve all data from the remote server
     *
     * @return array
     */
    public function findAll();
}