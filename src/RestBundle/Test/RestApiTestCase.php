<?php

namespace RestBundle\Test;


use AppBundle\Command\Utils\CustomUserManipulator;
use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Model\UserInterface;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Helper\FormatterHelper;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\DomCrawler\Crawler;

class RestApiTestCase extends KernelTestCase
{
    private static $staticClient;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var array
     */
    private static $container = array();

    /**
     * @var FormatterHelper
     */
    private $formatterHelper;

    private $output;

    /**
     * @var User
     */
    protected $currentUser;

    const USER_DEFAULT_FIRSTNAME = 'Andy';
    const USER_DEFAULT_LASTNAME = 'Pandy';
    const USER_DEFAULT_USERNAME = 'utidevid';
    const USER_DEFAULT_PASSWORD = 'password';
    const USER_DEFAULT_EMAIL = 'andy@gmail.com';

    public static function setUpBeforeClass()
    {
        $stack = HandlerStack::create();
        $stack->push(Middleware::history(self::$container));
        $stack->push(Middleware::mapRequest(function (RequestInterface $request) {
            $path = $request->getUri()->getPath();

            if (strpos($path, '/api') === 0) {
                return $request->withUri($request->getUri()->withPath('/app_test.php' . $path));
            }
            
            return $request;
        }));

        $baseUrl = getenv('TEST_BASE_URL');

        self::$staticClient = new Client([
            'base_uri' => $baseUrl,
            'http_errors' => false,
            'handler' => $stack
        ]);

        self::bootKernel();
    }

    public function setUp()
    {
        $this->client = self::$staticClient;
        $this->purgeDatabase();
        $this->currentUser = $this->createUser(
            'Raleigh',
            'Ale',
            'root',
            'root',
            'raleigh.ale@gmail.com'
        );
    }

    public function tearDown()
    {
        // Purposefully overriding so Symfony's kernel isn't shutdown
        $this->getService('doctrine')->getManager()->clear();
    }


    /**
     * Get JWT
     *
     * @param $username
     * @return array
     */
    protected function getAuthorizationHeaders($username, array $headers = array())
    {
        $token = $this->getService('lexik_jwt_authentication.encoder')
            ->encode(['username' => $username]);

        $headers['Authorization'] = 'Bearer ' . $token;

        return $headers;
    }

    /**
     * Purge database
     */
    protected function purgeDatabase()
    {
        $purger = new ORMPurger($this->getService('doctrine')->getManager());
        $purger->purge();
    }

    /**
     * Get service by ID
     *
     * @param $id
     * @return object
     */
    protected function getService($id) {
        return self::$kernel->getContainer()->get($id);
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager()
    {
        return $this->getService('doctrine.orm.entity_manager');
    }

    protected function onNotSuccessfulTest(\Throwable $e)
    {
        if (self::$container && self::getLastResponse()) {
            $this->printDebug('');
            $this->printDebug('<error>Failure!</error> when making the following request:');
            $this->printLastRequestUrl();
            $this->printDebug('');

            $this->debugResponse(self::getLastResponse());
        }

        throw $e;
    }

    /**
     * Print last request url
     */
    protected function printLastRequestUrl()
    {
        /** @var RequestInterface $lastRequest */
        $lastRequest = end(self::$container)['request'];

        if ($lastRequest) {
            $this->printDebug(sprintf('<comment>%s</comment>: <info>%s</info>',
                $lastRequest->getMethod(), $lastRequest->getUri()));
        } else {
            $this->printDebug('No request was made.');
        }
    }

    /**
     * Pring a message out - useful for debugging
     * @param $msg
     */
    protected function printDebug($msg)
    {
        if ($this->output === null) {
            $this->output = new ConsoleOutput();
        }

        $this->output->writeln($msg);
    }

    /**
     * Get start line and headers - original function is deprecated in a new version of GuzzleHttp
     *
     * @param ResponseInterface $response
     */
    private function getStartLineAndHeaders(ResponseInterface $response)
    {
        $this->printDebug($response->getStatusCode());
        $this->printDebug($response->getReasonPhrase());
        $this->printDebug($response->getProtocolVersion());

        foreach ($response->getHeaders() as $name => $values) {
            $this->printDebug($name . ': ' . implode(', ', $values));
        }
    }

    /**
     * Recognises Symfony's error page and extracts the important parts
     * You don't have to stare at a giant HTML page in the terminal
     *
     * @param ResponseInterface $response
     */
    protected function debugResponse(ResponseInterface $response)
    {

        $this->getStartLineAndHeaders($response);
        $body = (string) $response->getBody();

        $contentType = $response->getHeader('Content-Type')[0];

        if ($contentType == 'application/json' || strpos($contentType, '+json') !== false) {
            $data = json_decode($body);
            if ($data === null) {
                // invalid JSON!
                $this->printDebug($body);
            } else {
                // valid JSON, print it pretty
                $this->printDebug(json_encode($data, JSON_PRETTY_PRINT));
            }
        } else {
            // the response is HTML - see if we should print all of it or some of it
            $isValidHtml = strpos($body, '</body>') !== false;
            if ($isValidHtml) {
                $this->printDebug('');
                $crawler = new Crawler($body);
                // very specific to Symfony's error page
                $isError = $crawler->filter('#traces-0')->count() > 0
                    || strpos($body, 'looks like something went wrong') !== false;
                if ($isError) {
                    $this->printDebug('There was an Error!!!!');
                    $this->printDebug('');
                } else {
                    $this->printDebug('HTML Summary (h1 and h2):');
                }
                // finds the h1 and h2 tags and prints them only
                foreach ($crawler->filter('h1, h2')->extract(array('_text')) as $header) {
                    // avoid these meaningless headers
                    if (strpos($header, 'Stack Trace') !== false) {
                        continue;
                    }
                    if (strpos($header, 'Logs') !== false) {
                        continue;
                    }
                    // remove line breaks so the message looks nice
                    $header = str_replace("\n", ' ', trim($header));
                    // trim any excess whitespace "foo   bar" => "foo bar"
                    $header = preg_replace('/(\s)+/', ' ', $header);
                    if ($isError) {
                        $this->printErrorBlock($header);
                    } else {
                        $this->printDebug($header);
                    }
                }
                $profilerUrl = $response->getHeader('X-Debug-Token-Link');
                if ($profilerUrl) {
                    $fullProfilerUrl = $response->getHeader('Host') . $profilerUrl;
                    $this->printDebug('');
                    $this->printDebug(sprintf(
                        'Profiler URL: <comment>%s</comment>',
                        $fullProfilerUrl
                    ));
                }
                // an extra line for spacing
                $this->printDebug('');
            } else {
                $this->printDebug($body);
            }
        }
    }

    /**
     * Print a debugging message out in a big red block
     * @param $msg
     */
    protected function printErrorBlock($msg)
    {
        if ($this->formatterHelper === null) {
            $this->formatterHelper = new FormatterHelper();
        }

        $output = $this->formatterHelper->formatBlock($msg, 'bg=red;fg=white', true);

        $this->printDebug($output);
    }

    /**
     * Get last reposponse from the history container
     *
     * @return mixed
     */
    public static function getLastResponse()
    {
        return end(self::$container)['response'];
    }

    /**
     * Helps when comparing expected URIs
     * e.g. in testGet a return URI has no app_test.php prefix because of routing
     *
     * @param $uri
     * @return string
     */
    protected function adjustUri($uri)
    {
        return '/app_test.php' . $uri;
    }

    /**
     * Create a user
     *
     * @param string $fname
     * @param string $lname
     * @param string $username
     * @param string $password
     * @param string $email
     * @param bool $inactive
     * @param bool $superadmin
     *
     * @return UserInterface
     */
    protected function createUser($fname = self::USER_DEFAULT_FIRSTNAME, $lname = self::USER_DEFAULT_LASTNAME,
                                  $username = self::USER_DEFAULT_USERNAME, $password = self::USER_DEFAULT_PASSWORD,
                                  $email = self::USER_DEFAULT_EMAIL, $inactive = false, $superadmin = false)
    {
        return $this->getService(CustomUserManipulator::class)
            ->createCustomUser($fname, $lname, $username, $password, $email, !$inactive, $superadmin);
    }
}