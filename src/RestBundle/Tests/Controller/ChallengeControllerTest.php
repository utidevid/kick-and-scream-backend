<?php

namespace RestBundle\Tests\Controller;

use Doctrine\Common\Util\Inflector;
use RestBundle\Api\ApiProblem;
use RestBundle\Entity\Category;
use RestBundle\Entity\Challenge;
use RestBundle\Entity\GameUser;
use RestBundle\Test\RestApiTestCase;

class ChallengeControllerTest extends RestApiTestCase
{
    /**
     * @return Category
     */
    public function createCategory()
    {
        return $this->mockEntity(new Category(), array(
            'title' => 'Impressive Title ' . rand(1,100),
            'language' => 'en',
            'sku' => 'cat_' . rand(1,999),
            'object_id' => 'cat' . rand(100,999)  .'xyz' . rand(0,9)
        ));
    }

    /**
     * @return GameUser
     */
    public function createGameUser()
    {
        return $this->mockEntity(new GameUser(), array(
            'primary_email' => 'tempuser' . rand(1,999). '@tempemail.com',
            'device_name' => 'Temp Device Name',
            'device_id' => 'Temp Device ID ' . rand(1,9999),
            'object_id' => 'agu' . rand(100,999)  .'xyz' . rand(0,9)
        ));
    }

    /**
     * Create and return new challenge object
     *
     * @param Category|null $category
     * @param GameUser|null $gameUser
     * @param null $index
     * @param array $customChallengeData
     *
     * @return array
     */
    public function createChallenge(Category $category = null, GameUser $gameUser = null, $index = null, array $customChallengeData = array())
    {
        if ($category == null) {
            $category = $this->createCategory();
        }

        if ($gameUser == null) {
            $gameUser = $this->createGameUser();
        }

        // Create challenge entity
        $challengeData = array(
            'title' => 'Impressive Title ' . ($index !== null ? $index : rand(1,999)),
            'content' => 'This is a very impressive content.',
            'points' => rand(1,15),
            'gender' => Challenge::GENDER_UNISEX,
            'code' => Challenge::CODE_BASIC,
            'type' => Challenge::TYPE_KICK,
            'source' => Challenge::SOURCE_GENERIC,
            'moderated' => Challenge::MODERATED,
            'min_players_count' => 2,
            'category' => $category,
            'game_user' => $gameUser,
            'rating' => 22,
            'object_id' => ($index !== null ? (string) $index : 'abc' . rand(100,999) .'xyz' . rand(0,9))
        );

        if ($customChallengeData != null) {
            $challengeData = array_merge($challengeData, $customChallengeData);
        }

        $challenge = $this->mockEntity(new Challenge(), $challengeData);

        $challengeData['id'] = $challenge->getId();
        $challengeData['category'] = $category->getId();
        $challengeData['game_user'] = $gameUser->getId();

        return $challengeData;
    }

    /**
     * Mocks an entity from data
     * 
     * @param $entity
     * @param $data
     * @return object
     */
    public function mockEntity($entity, array $data)
    {
        $entity = $this->assembleEntityFromArray($entity, $data);
        $this->saveEntity($entity);

        return $entity;
    }

    private function saveEntity($entity)
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }

    /**
     * Assembles an entity from array of data
     *
     * @param $entity - entity to assemble from data
     * @param $data - data to imprint into entity
     * @return object
     */
    private function assembleEntityFromArray($entity, array $data)
    {
        foreach ($data as $propertyName => $propertyValue) {
            $propertySetter = 'set' . ucfirst(Inflector::camelize($propertyName));
            $entity->$propertySetter($propertyValue);
        }

        return $entity;
    }


    /**
     * @param array $sentData - data sent to API
     * @param array $returnedData data returned from API
     * @param array $ignorePropertyKeys - (optional) which sent property's keys to ignore
     */
    protected function matchSendAndReturnedData(array $sentData, array $returnedData, array $ignorePropertyKeys = [])
    {
        foreach ($sentData as $sentPropertyKey => $sendPropertyValue) {
            if (in_array($sentPropertyKey, $ignorePropertyKeys)) {
                continue;
            }

            $this->assertArrayHasKey($sentPropertyKey, $returnedData);
            $this->assertEquals($sendPropertyValue, $returnedData[$sentPropertyKey]);
        }
    }

    public function testPost()
    {
        $category = $this->createCategory();
        $gameUser = $this->createGameUser();

        $data = array(
            'title' => 'Impressive Title ' . rand(1,999),
            'content' => 'This is a very impressive content.',
            'points' => rand(1,15),
            'gender' => Challenge::GENDER_UNISEX,
            'code' => Challenge::CODE_BASIC,
            'type' => Challenge::TYPE_KICK,
            'source' => Challenge::SOURCE_GENERIC,
            'moderated' => Challenge::MODERATED,
            'min_players_count' => 2,
            'category' => $category->getId(),
            'game_user' => $gameUser->getId(),
            'native_id' => 2,
        );

        $resource = $this->client->post('/api/challenges', [
            'body' => json_encode($data),
            'headers' => $this->getAuthorizationHeaders($this->currentUser->getUsername())
        ]);
        $response = json_decode($resource->getBody(), true);

        $this->assertEquals(201, $resource->getStatusCode());
        $this->matchSendAndReturnedData($data, $response, ['native_id']);
        $this->assertEquals(null, $response['native_id']);
    }

    public function testGet()
    {
        $challenge = $this->createChallenge();

        $resource = $this->client->get('/api/challenges/' . $challenge['id'], [
            'headers' => $this->getAuthorizationHeaders($this->currentUser->getUsername())
        ]);
        $response = json_decode($resource->getBody(), true);

        $this->assertEquals(200, $resource->getStatusCode());
        $this->assertEquals('application/hal+json', $resource->getHeader('Content-Type')[0]);
        $this->matchSendAndReturnedData($challenge, $response);

        $this->assertArrayHasKey('_links', $response);
        $this->assertArrayHasKey('self', $response['_links']);
        $this->assertArrayHasKey('href', $response['_links']['self']);
        $this->assertEquals($this->adjustUri('/api/challenges/' . $challenge['id']), $response['_links']['self']['href']);
    }

    public function testGetDeep()
    {
        $category = $this->createCategory();
        $gameUser = $this->createGameUser();

        $challenge = $this->createChallenge($category, $gameUser);

        $resource = $this->client->get('/api/challenges/' . $challenge['id'] . '?deep=1',[
            'headers' => $this->getAuthorizationHeaders($this->currentUser->getUsername())
        ]);
        $response = json_decode($resource->getBody(), true);

        $this->assertEquals(200, $resource->getStatusCode());
        $this->matchSendAndReturnedData($challenge, $response, ['category', 'game_user']);

        $this->assertArrayHasKey('category', $response);
        $this->assertInternalType('array', $response['category']);
        $this->assertArrayHasKey('title', $response['category']);
        $this->assertEquals($category->getTitle(), $response['category']['title']);

        $this->assertArrayHasKey('game_user', $response);
        $this->assertInternalType('array', $response['game_user']);
        $this->assertArrayHasKey('primary_email', $response['game_user']);
        $this->assertEquals($gameUser->getPrimaryEmail(), $response['game_user']['primary_email']);
    }

    public function testList()
    {
        $category = $this->createCategory();
        $gameUser = $this->createGameUser();

        $challenge1 = $this->createChallenge($category, $gameUser, 1);
        $challenge2 = $this->createChallenge($category, $gameUser, 2);
        $challenge3 = $this->createChallenge($category, $gameUser, 3);
        
        $resource = $this->client->get('/api/challenges',[
            'headers' => $this->getAuthorizationHeaders($this->currentUser->getUsername())
        ]);
        $response = json_decode($resource->getBody(), true);

        $this->assertEquals(200, $resource->getStatusCode());
        $this->assertEquals('application/hal+json', $resource->getHeader('Content-Type')[0]);
        $this->assertInternalType('array', $response);
        $this->assertArrayHasKey('items', $response);
        $this->assertInternalType('array', $response['items']);
        $this->assertCount(3, $response['items']);
        $this->matchSendAndReturnedData($challenge1, $response['items'][0]);
        $this->matchSendAndReturnedData($challenge2, $response['items'][1]);
        $this->matchSendAndReturnedData($challenge3, $response['items'][2]);
    }
    
    public function testPatch()
    {
        $challenge = $this->createChallenge();

        $data = [
            'title' => 'Outstanding Title ' . rand(1, 999),
            'points' => 5,
            'object_id' => 'abc123xyz4',
            'game_user' => 2,
            'native_id' => 3,
        ];

        $resource = $this->client->patch('/api/challenges/' . $challenge['id'], [
            'body' => json_encode($data),
            'headers' => $this->getAuthorizationHeaders($this->currentUser->getUsername())
        ]);
        $response = json_decode($resource->getBody(), true);

        $this->assertEquals(200, $resource->getStatusCode());
        $this->matchSendAndReturnedData(array_merge($challenge, $data), $response, ['native_id', 'game_user','object_id']);

        $this->assertEquals($challenge['game_user'], $response['game_user']);
        $this->assertEquals(null, $response['native_id']);
        $this->assertEquals($challenge['object_id'], $response['object_id']);
    }

    public function testPut()
    {
        $challenge = $this->createChallenge();

        $data = array_merge($challenge, [
            'title' => 'Outstanding Title ' . rand(1, 999),
            'content' => 'The new content will serve it purpose throughout the lifetime of the challenge!',
            'type' => Challenge::TYPE_SCREAM,
            'points' => 5,
            'native_id' => 3
        ]);

        // Purposefully unset the following data, as they should be interpreted as null by the server
        unset($data['game_user']);
        unset($data['rating']);
        unset($data['object_id']);

        $resource = $this->client->put('/api/challenges/' . $challenge['id'], [
            'body' => json_encode($data),
            'headers' => $this->getAuthorizationHeaders($this->currentUser->getUsername())
        ]);
        $response = json_decode($resource->getBody(), true);

        $this->assertEquals(200, $resource->getStatusCode());
        $this->matchSendAndReturnedData($data, $response, ['native_id']);
        $this->assertEquals(null, $response['native_id']);

        // Check if unset data are returned are set to null by the server
        $this->assertEquals(null, $response['object_id']);
        $this->assertEquals(null, $response['rating']);
        $this->assertEquals(null, $response['game_user']);
    }

    public function testRateChallengeUp()
    {
        $challenge = $this->createChallenge();

        $resource = $this->client->put('/api/challenges/' . $challenge['id'] . '/rate/up', [
            'body' => json_encode(['rating' => 5]),
            'headers' => $this->getAuthorizationHeaders($this->currentUser->getUsername())
        ]);
        $response = json_decode($resource->getBody(), true);

        $this->assertEquals(200, $resource->getStatusCode());

        $this->assertInternalType('array', $response);
        $this->assertArrayHasKey('rating', $response);
        $this->assertInternalType('int', $response['rating']);
        $this->assertEquals($challenge['rating']+5, $response['rating']);
    }

    public function testRateChallengeDown()
    {
        $challenge = $this->createChallenge();

        $resource = $this->client->put('/api/challenges/' . $challenge['id'] . '/rate/down', [
            'body' => json_encode(['rating' => 5]),
            'headers' => $this->getAuthorizationHeaders($this->currentUser->getUsername())
        ]);
        $response = json_decode($resource->getBody(), true);

        $this->assertEquals(200, $resource->getStatusCode());

        $this->assertInternalType('array', $response);
        $this->assertArrayHasKey('rating', $response);
        $this->assertInternalType('int', $response['rating']);
        $this->assertEquals($challenge['rating']-5, $response['rating']);
    }

    public function testPutForCreate()
    {
        $category = $this->createCategory();
        $gameUser = $this->createGameUser();

        $data = array(
            'title' => 'Impressive Title ' . rand(1,999),
            'content' => 'This is a very impressive content.',
            'points' => rand(1,15),
            'gender' => Challenge::GENDER_UNISEX,
            'code' => Challenge::CODE_BASIC,
            'type' => Challenge::TYPE_KICK,
            'source' => Challenge::SOURCE_GENERIC,
            'moderated' => Challenge::MODERATED,
            'min_players_count' => 2,
            'category' => $category->getId(),
            'game_user' => $gameUser->getId(),
            'native_id' => 2,
        );

        $resource = $this->client->put('/api/challenges/1', [
            'body' => json_encode($data),
            'headers' => $this->getAuthorizationHeaders($this->currentUser->getUsername())
        ]);
        $response = json_decode($resource->getBody(), true);

        $this->assertEquals(201, $resource->getStatusCode());
        $this->matchSendAndReturnedData($data, $response, ['native_id']);
        $this->assertEquals(null, $response['native_id']);
    }

    public function testDelete()
    {
        $challenge = $this->createChallenge();

        $resource = $this->client->delete('/api/challenges/' . $challenge['id'],[
            'headers' => $this->getAuthorizationHeaders($this->currentUser->getUsername())
        ]);
        $response = json_decode($resource->getBody(), true);

        $this->assertEquals(204, $resource->getStatusCode());
        $this->assertEquals(null, $response);
    }

    public function testValidationErrors()
    {
        $category = $this->createCategory();
        $gameUser = $this->createGameUser();

        $data = array(
            //'title' => 'Impressive Title ' . rand(1,999),
            'content' => 'This is a very impressive content.',
            'points' => rand(1,15),
            'gender' => Challenge::GENDER_UNISEX,
            'code' => Challenge::CODE_BASIC,
            'type' => Challenge::TYPE_KICK,
            'source' => Challenge::SOURCE_GENERIC,
            'moderated' => Challenge::MODERATED,
            'min_players_count' => 2,
            'category' => $category->getId(),
            'game_user' => $gameUser->getId()
        );

        $resource = $this->client->post('/api/challenges', [
            'body' => json_encode($data),
            'headers' => $this->getAuthorizationHeaders($this->currentUser->getUsername())
        ]);
        $response = json_decode($resource->getBody(), true);

        $this->assertEquals(400, $resource->getStatusCode());
        $this->assertEquals('application/problem+json', $resource->getHeader('Content-Type')[0]);

        $this->assertArrayHasKey('type', $response);
        $this->assertArrayHasKey('title', $response);
        $this->assertArrayHasKey('status', $response);
        $this->assertArrayHasKey('errors', $response);
        $this->assertArrayHasKey('detail', $response);

        $this->assertArrayHasKey('title', $response['errors']);
        $this->assertEquals('This value should not be blank.', $response['errors']['title'][0]);

        foreach ($data as $propertyKey => $propertyValue) {
            $this->assertArrayNotHasKey($propertyKey, $response['errors']);
        }
    }

    public function testInvalidJson()
    {
        $invalidJson = '{"title": """content":"a"}';

        $resource = $this->client->post('/api/challenges', [
            'body' => $invalidJson,
            'headers' => $this->getAuthorizationHeaders($this->currentUser->getUsername(), [
                'Content-Type' => 'text'
            ])
        ]);
        $response = json_decode($resource->getBody(), true);

        $this->assertEquals(400, $resource->getStatusCode());
        $this->assertEquals('application/problem+json', $resource->getHeader('Content-Type')[0]);

        $this->assertArrayHasKey('type', $response);
        $this->assertArrayHasKey('title', $response);
        $this->assertArrayHasKey('status', $response);
    }

    public function testNotFoundParentEntity()
    {
        $resource = $this->client->get('/api/challenges/1',[
            'headers' => $this->getAuthorizationHeaders($this->currentUser->getUsername())
        ]);
        $response = json_decode($resource->getBody(), true);

        $this->assertEquals(404, $resource->getStatusCode());
        $this->assertEquals('application/problem+json', $resource->getHeader('Content-Type')[0]);

        $this->assertArrayHasKey('status', $response);
        $this->assertArrayHasKey('type', $response);
        $this->assertArrayHasKey('title', $response);
        $this->assertArrayHasKey('detail', $response);
        $this->assertArrayHasKey('entity', $response);
        $this->assertArrayHasKey('id', $response['entity']);
        $this->assertArrayHasKey('name', $response['entity']);

        $this->assertEquals(404, $response['status']);
        $this->assertEquals(ApiProblem::TYPE_ABOUT_BLANK, $response['type']);
        $this->assertEquals(ApiProblem::getStatusCodeTitle(404), $response['title']);

        $this->assertEquals('Challenge', $response['entity']['name']);
        $this->assertEquals(1, $response['entity']['id']);
    }

    public function testNotFoundSubEntity()
    {
        $category = $this->createCategory();

        $data = array(
            'title' => 'Impressive Title ' . rand(1,999),
            'content' => 'This is a very impressive content.',
            'points' => rand(1,15),
            'gender' => Challenge::GENDER_UNISEX,
            'code' => Challenge::CODE_BASIC,
            'type' => Challenge::TYPE_KICK,
            'source' => Challenge::SOURCE_GENERIC,
            'moderated' => Challenge::MODERATED,
            'min_players_count' => 2,
            'category' => $category->getId(),
            'game_user' => 1,
        );

        $resource = $this->client->post('/api/challenges', [
            'body' => json_encode($data),
            'headers' => $this->getAuthorizationHeaders($this->currentUser->getUsername())
        ]);
        $response = json_decode($resource->getBody(), true);

        $this->assertEquals(404, $resource->getStatusCode());
        $this->assertEquals('application/problem+json', $resource->getHeader('Content-Type')[0]);

        $this->assertArrayHasKey('status', $response);
        $this->assertArrayHasKey('type', $response);
        $this->assertArrayHasKey('title', $response);
        $this->assertArrayHasKey('detail', $response);
        $this->assertArrayHasKey('sub_entity', $response);
        $this->assertArrayHasKey('id', $response['sub_entity']);
        $this->assertArrayHasKey('name', $response['sub_entity']);
        $this->assertArrayHasKey('entity', $response);
        $this->assertArrayHasKey('id', $response['entity']);
        $this->assertArrayHasKey('name', $response['entity']);
        
        $this->assertEquals(404, $response['status']);
        $this->assertEquals(ApiProblem::TYPE_ABOUT_BLANK, $response['type']);
        $this->assertEquals(ApiProblem::getStatusCodeTitle(404), $response['title']);

        $this->assertEquals('GameUser', $response['sub_entity']['name']);
        $this->assertEquals($data['game_user'], $response['sub_entity']['id']);

        $this->assertEquals('Challenge', $response['entity']['name']);
        $this->assertEquals('new', $response['entity']['id']);
    }

    public function testNotFoundSubEntityForPatch()
    {
        $challenge = $this->createChallenge();

        $data = [
            'category' => ++$challenge['category']
        ];

        $resource = $this->client->patch('/api/challenges/'  . $challenge['id'], [
            'body' => json_encode($data),
            'headers' => $this->getAuthorizationHeaders($this->currentUser->getUsername())
        ]);
        $response = json_decode($resource->getBody(), true);

        $this->assertEquals(404, $resource->getStatusCode());
        $this->assertEquals('application/problem+json', $resource->getHeader('Content-Type')[0]);

        $this->assertArrayHasKey('status', $response);
        $this->assertArrayHasKey('type', $response);
        $this->assertArrayHasKey('title', $response);
        $this->assertArrayHasKey('detail', $response);
        $this->assertArrayHasKey('sub_entity', $response);
        $this->assertArrayHasKey('id', $response['sub_entity']);
        $this->assertArrayHasKey('name', $response['sub_entity']);
        $this->assertArrayHasKey('entity', $response);
        $this->assertArrayHasKey('id', $response['entity']);
        $this->assertArrayHasKey('name', $response['entity']);

        $this->assertEquals(404, $response['status']);
        $this->assertEquals(ApiProblem::TYPE_ABOUT_BLANK, $response['type']);
        $this->assertEquals(ApiProblem::getStatusCodeTitle(404), $response['title']);

        $this->assertEquals('Category', $response['sub_entity']['name']);
        $this->assertEquals($data['category'], $response['sub_entity']['id']);

        $this->assertEquals('Challenge', $response['entity']['name']);
        $this->assertEquals($challenge['id'], $response['entity']['id']);
    }

    public function testDataTypes()
    {
        $challenge = $this->createChallenge();

        $data = [
            'category' => true,
            'rating' => '22'
        ];

        $resource = $this->client->patch('/api/challenges/'  . $challenge['id'], [
            'body' => json_encode($data),
            'headers' => $this->getAuthorizationHeaders($this->currentUser->getUsername())
        ]);
        $response = json_decode($resource->getBody(), true);

        $this->assertEquals(400, $resource->getStatusCode());
        $this->assertEquals('application/problem+json', $resource->getHeader('Content-Type')[0]);

        $this->assertArrayHasKey('status', $response);
        $this->assertArrayHasKey('type', $response);
        $this->assertArrayHasKey('title', $response);
        $this->assertArrayHasKey('detail', $response);
        $this->assertArrayHasKey('errors', $response);
        $this->assertCount(2, $response['errors']);
        $this->assertArrayHasKey('category', $response['errors']);
        $this->assertArrayHasKey('rating', $response['errors']);

        $this->assertEquals(400, $response['status']);
        $this->assertEquals(ApiProblem::INVALID_DATA_TYPE, $response['type']);

        $this->assertEquals('Field requires integer data type. Instead boolean type was passed.', $response['errors']['category'][0]);
        $this->assertEquals('Field requires integer or null data type. Instead string type was passed.', $response['errors']['rating'][0]);
    }

    public function testChallengesCollectionsPaginated()
    {
        $category = $this->createCategory();
        $gameUser = $this->createGameUser();

        $challengesCollection = array();

        for($i = 0; $i < 100; $i++) {
            $challengesCollection[] = $this->createChallenge($category, $gameUser, $i);
        }

        $totalItems = count($challengesCollection);

        $resource = $this->client->get('/api/challenges',[
            'headers' => $this->getAuthorizationHeaders($this->currentUser->getUsername())
        ]);
        $response = json_decode($resource->getBody(), true);

        $this->assertEquals(200, $resource->getStatusCode());
        $this->assertInternalType('array', $response);

        $this->assertArrayHasKey('items', $response);
        $this->assertCount(30, $response['items']);
        $this->assertArrayHasKey('title', $response['items'][15]);
        $this->assertEquals($challengesCollection[15]['title'], $response['items'][15]['title']);
        
        $this->assertArrayHasKey('count', $response);
        $this->assertEquals(30, $response['count']);

        $this->assertArrayHasKey('total', $response);
        $this->assertEquals($totalItems, $response['total']);

        $this->assertArrayHasKey('_links', $response);
        $this->assertArrayHasKey('first', $response['_links']);
        $this->assertArrayNotHasKey('prev', $response['_links']);
        $this->assertArrayHasKey('next', $response['_links']);
        $this->assertArrayHasKey('last', $response['_links']);


        $nextUrl = $response['_links']['next'];

        $resource = $this->client->get($nextUrl,[
            'headers' => $this->getAuthorizationHeaders($this->currentUser->getUsername())
        ]);
        $response = json_decode($resource->getBody(), true);

        $this->assertEquals(200, $resource->getStatusCode());
        $this->assertInternalType('array', $response);

        $this->assertArrayHasKey('items', $response);
        $this->assertCount(30, $response['items']);
        $this->assertArrayHasKey('title', $response['items'][15]);
        $this->assertEquals($challengesCollection[45]['title'], $response['items'][15]['title']);

        $this->assertArrayHasKey('count', $response);
        $this->assertEquals(30, $response['count']);

        $this->assertArrayHasKey('total', $response);
        $this->assertEquals($totalItems, $response['total']);

        $this->assertArrayHasKey('_links', $response);
        $this->assertArrayHasKey('first', $response['_links']);
        $this->assertArrayHasKey('prev', $response['_links']);
        $this->assertArrayHasKey('next', $response['_links']);
        $this->assertArrayHasKey('last', $response['_links']);


        $lastUrl = $response['_links']['last'];

        $resource = $this->client->get($lastUrl,[
            'headers' => $this->getAuthorizationHeaders($this->currentUser->getUsername())
        ]);
        $response = json_decode($resource->getBody(), true);

        $this->assertEquals(200, $resource->getStatusCode());
        $this->assertInternalType('array', $response);

        $this->assertArrayHasKey('items', $response);
        $this->assertCount(10, $response['items']);
        $this->assertArrayHasKey('title', $response['items'][9]);
        $this->assertEquals($challengesCollection[99]['title'], $response['items'][9]['title']);

        $this->assertArrayHasKey('count', $response);
        $this->assertEquals(10, $response['count']);

        $this->assertArrayHasKey('total', $response);
        $this->assertEquals($totalItems, $response['total']);

        $this->assertArrayHasKey('_links', $response);
        $this->assertArrayHasKey('first', $response['_links']);
        $this->assertArrayHasKey('prev', $response['_links']);
        $this->assertArrayNotHasKey('next', $response['_links']);
        $this->assertArrayHasKey('last', $response['_links']);
    }

    public function testChallengesCollectionFiltering()
    {
        $category = $this->createCategory();
        $gameUser = $this->createGameUser();

        $challengesCollection = array();

        for($i = 0; $i < 10; $i++) {
            $challengesCollection[] = $this->createChallenge($category, $gameUser, $i);
        }

        $willNotFindChallenge = $this->createChallenge($category, $gameUser, $i, [
            'title' => 'Will not find it',
            'content' => 'Can never be found'
        ]);
        
        $resource = $this->client->get('/api/challenges?filter=impressive',[
            'headers' => $this->getAuthorizationHeaders($this->currentUser->getUsername())
        ]);
        $response = json_decode($resource->getBody(), true);

        $this->assertEquals(200, $resource->getStatusCode());
        $this->assertInternalType('array', $response);

        $this->assertArrayHasKey('items', $response);
        $this->assertCount(10, $response['items']);

        foreach($response['items'] as $challenge) {
            $this->assertArrayHasKey('title', $challenge);
            $this->assertNotEquals($willNotFindChallenge['title'], $challenge['title']);
            $this->assertNotEquals($willNotFindChallenge['content'], $challenge['content']);
        }

        $this->assertArrayHasKey('count', $response);
        $this->assertEquals(10, $response['count']);

        $this->assertArrayHasKey('total', $response);
        $this->assertEquals(10, $response['total']);

        $this->assertArrayHasKey('_links', $response);
        $this->assertArrayHasKey('first', $response['_links']);
        $this->assertArrayNotHasKey('prev', $response['_links']);
        $this->assertArrayNotHasKey('next', $response['_links']);
        $this->assertArrayHasKey('last', $response['_links']);
    }

    public function testRequiredAuthentication()
    {
        $category = $this->createCategory();
        $gameUser = $this->createGameUser();

        $data = array(
            'title' => 'Impressive Title ' . rand(1,999),
            'content' => 'This is a very impressive content.',
            'points' => rand(1,15),
            'gender' => Challenge::GENDER_UNISEX,
            'code' => Challenge::CODE_BASIC,
            'type' => Challenge::TYPE_KICK,
            'source' => Challenge::SOURCE_GENERIC,
            'moderated' => Challenge::MODERATED,
            'min_players_count' => 2,
            'category' => $category->getId(),
            'game_user' => $gameUser->getId(),
            'native_id' => 2,
        );

        $resource = $this->client->post('/api/challenges', [
            'body' => json_encode($data)
        ]);
        $response = json_decode($resource->getBody(), true);

        $this->assertEquals('application/problem+json', $resource->getHeader('Content-Type')[0]);
        $this->assertEquals(403, $resource->getStatusCode());

        $this->assertArrayHasKey('title', $response);
        $this->assertEquals('Forbidden', $response['title']);

        $this->assertArrayHasKey('detail', $response);
        $this->assertEquals('Missing token. Create a new one at ' . $this->adjustUri('/api/token'), $response['detail']);
    }

    public function testBadToken()
    {
        $category = $this->createCategory();
        $gameUser = $this->createGameUser();

        $data = array(
            'title' => 'Impressive Title ' . rand(1,999),
            'content' => 'This is a very impressive content.',
            'points' => rand(1,15),
            'gender' => Challenge::GENDER_UNISEX,
            'code' => Challenge::CODE_BASIC,
            'type' => Challenge::TYPE_KICK,
            'source' => Challenge::SOURCE_GENERIC,
            'moderated' => Challenge::MODERATED,
            'min_players_count' => 2,
            'category' => $category->getId(),
            'game_user' => $gameUser->getId(),
            'native_id' => 2,
        );

        $resource = $this->client->post('/api/challenges', [
            'body' => json_encode($data),
            'headers' => [
                'Authorization' => 'Bearer WRONG'
            ]
        ]);
        $response = json_decode($resource->getBody(), true);

        $this->assertEquals('application/problem+json', $resource->getHeader('Content-Type')[0]);
        $this->assertEquals(401, $resource->getStatusCode());

        $this->assertArrayHasKey('title', $response);
        $this->assertEquals('Unauthorized', $response['title']);

        $this->assertArrayHasKey('detail', $response);
        $this->assertEquals('Your token is invalid, please generate a new one at ' . $this->adjustUri('/api/token'), $response['detail']);
    }
}