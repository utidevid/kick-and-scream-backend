<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 9/26/2017
 * Time: 1:38 PM
 */

namespace RestBundle\Tests\Controller;

use RestBundle\Test\RestApiTestCase;
use Symfony\Component\HttpFoundation\Response;

class TokenControllerTest extends RestApiTestCase
{
    public function testPostCreateToken()
    {
        $this->createUser();

        $request = $this->client->post('/api/token', [
            'form_params' => [
                '_username' => self::USER_DEFAULT_USERNAME,
                '_password' => self::USER_DEFAULT_PASSWORD
            ]
        ]);
        $response = json_decode($request->getBody(), true);

        $this->assertEquals(200, $request->getStatusCode());
        $this->assertArrayHasKey('token', $response);
    }

    public function testPostTokenInvalidUsername()
    {
        $this->createUser();

        $resource = $this->client->post('/api/token', [
            'form_params' => [
                '_username' => self::USER_DEFAULT_USERNAME,
                '_password' => 'asdaasdasd'
            ]
        ]);
        $response = json_decode($resource->getBody(), true);

        $this->assertEquals('application/problem+json', $resource->getHeader('Content-Type')[0]);
        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $resource->getStatusCode());

        $this->assertArrayHasKey('detail', $response);
        $this->assertEquals('Bad credentials, please verify that your username/password are correctly set.', $response['detail']);
        $this->assertArrayNotHasKey('token', $response);
    }
}
