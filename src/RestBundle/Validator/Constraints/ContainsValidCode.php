<?php

namespace RestBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class ContainsValidCode extends Constraint
{
    /**
     * @var string error messages sticks to a field
     */
    public $errorMsgMappedTo;

    /**
     * @var string
     */
    public $message = 'Required placeholders {{ placeholders }} are missing in {{ searchName }} field';


    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

    public function validatedBy()
    {
        return 'rest.validator.contains_valid_code';
    }
}