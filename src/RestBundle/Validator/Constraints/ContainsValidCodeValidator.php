<?php

namespace RestBundle\Validator\Constraints;

use RestBundle\Entity\Challenge;
use Symfony\Component\Validator\ConstraintValidator;

class ContainsValidCodeValidator extends ConstraintValidator
{

    const CODE_VARIABLE_PLAYER = "{" . Challenge::CODE_VARIABLE_PLAYER . "}";
    const CODE_VARIABLE_PLAYER_ONE = "{" . Challenge::CODE_VARIABLE_PLAYER_ONE . "}";
    const CODE_VARIABLE_PLAYER_TWO = "{" . Challenge::CODE_VARIABLE_PLAYER_TWO . "}";
    const CODE_VARIABLE_MALE = "{" . Challenge::CODE_VARIABLE_MALE . "}";
    const CODE_VARIABLE_FEMALE = "{" . Challenge::CODE_VARIABLE_FEMALE . "}";

    const VALID_PLACEHOLDERS_FOR_CODES = [
        [
            // Codes
            [
                Challenge::CODE_SOMEONE_OF_THE_SAME_SEX,
                Challenge::CODE_SOMEONE_OF_THE_OPPOSITE_SEX,
                Challenge::CODE_ANY_PLAYER,
            ],
            // Required placeholders
            [
                self::CODE_VARIABLE_PLAYER,
            ]
        ],
        [
            [
                Challenge::CODE_TWO_FEMALES,
                Challenge::CODE_TWO_MALES,
            ],
            [
                self::CODE_VARIABLE_PLAYER_ONE,
                self::CODE_VARIABLE_PLAYER_TWO,
            ]
        ],
        [
            [
                Challenge::CODE_MALE_AND_FEMALE,
                Challenge::CODE_TWO_MALES,
            ],
            [
                self::CODE_VARIABLE_FEMALE,
                self::CODE_VARIABLE_MALE,
            ]
        ],
    ];

    protected function getMissingPlaceholdersAgainstCode($code, $searchedContent)
    {
        $missingPlaceholders = array();

        foreach (self::VALID_PLACEHOLDERS_FOR_CODES as $selection) {
            list($codes, $requiredPlaceholders) = $selection;

            // Select code to choose placeholders
            if (in_array($code, $codes)) {
                // Check if code's placeholders exist in the searched content
                foreach ($requiredPlaceholders as $placeholder) {
                    if (!(strpos($searchedContent, $placeholder) !== false)) {
                        $missingPlaceholders[] = $placeholder;
                    }
                }
            }
        }

        return $missingPlaceholders;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param Challenge $challenge The value that should be validated
     * @param \Symfony\Component\Validator\Constraint $constraint The constraint for the validation
     */
    public function validate($challenge, \Symfony\Component\Validator\Constraint $constraint)
    {

        $missingPlaceholders = $this->getMissingPlaceholdersAgainstCode($challenge->getCode(), $challenge->getContent());

        if (count($missingPlaceholders) > 0) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ placeholders }}', implode(' ', $missingPlaceholders))
                ->atPath($constraint->errorMsgMappedTo)
                ->addViolation();
        }

        /*$searchedContent = $challenge->getContent();
        $code = $challenge->getCode();

        $placeholders = array();

        if (($code === Challenge::CODE_SOMEONE_OF_THE_SAME_SEX || $code === Challenge::CODE_SOMEONE_OF_THE_OPPOSITE_SEX || $code === Challenge::CODE_ANY_PLAYER) &&
            !(strpos($searchedContent, self::CODE_VARIABLE_PLAYER) !== false)) {
            $placeholders[] = self::CODE_VARIABLE_PLAYER;
        }

        if (($code === Challenge::CODE_TWO_FEMALES || $code === Challenge::CODE_TWO_MALES) &&
            !(strpos($searchedContent, self::CODE_VARIABLE_PLAYER_ONE) !== false)) {
            $placeholders[] = self::CODE_VARIABLE_PLAYER_ONE;
        }

        if (($code === Challenge::CODE_TWO_FEMALES || $code === Challenge::CODE_TWO_MALES) &&
            !(strpos($searchedContent, self::CODE_VARIABLE_PLAYER_TWO) !== false)) {
            $placeholders[] = self::CODE_VARIABLE_PLAYER_TWO;
        }

        if ($code === Challenge::CODE_MALE_AND_FEMALE &&
            !(strpos($searchedContent, self::CODE_VARIABLE_MALE) !== false)) {
            $placeholders[] = self::CODE_VARIABLE_MALE;
        }

        if ($code === Challenge::CODE_MALE_AND_FEMALE &&
            !(strpos($searchedContent, self::CODE_VARIABLE_FEMALE) !== false)) {
            $placeholders[] = self::CODE_VARIABLE_FEMALE;
        }*/
    }
}